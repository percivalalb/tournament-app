CREATE TABLE accounts (
    account_id      VARCHAR(56) NOT NULL,
    summoner_id     VARCHAR(63) NOT NULL,
    puuid           VARCHAR(78) NOT NULL,
    player_id       INT(10) NOT NULL,
    name            VARCHAR(255) NOT NULL,
    PRIMARY KEY (account_id)
);

CREATE UNIQUE INDEX index_account_id ON accounts (account_id);
CREATE INDEX index_summoner_id ON accounts (id);
CREATE INDEX index_player_id ON accounts (player_id);

CREATE TABLE players (
    player_id       INT(10) NOT NULL AUTO_INCREMENT,
    name            VARCHAR(255) NOT NULL,
    rank            INT(4) NOT NULL DEFAULT 1000,
    title           VARCHAR(255),
    PRIMARY KEY (player_id)
);
CREATE UNIQUE INDEX index_player_id ON players (player_id);

CREATE TABLE roles (
    summoner_id          VARCHAR(63) NOT NULL,
    primary_role         ENUM ('top', 'jungle', 'mid', 'adc', 'support') NOT NULL,
    secondary_role       ENUM ('top', 'jungle', 'mid', 'adc', 'support') NOT NULL,
    fill                 TINYINT(1) UNSIGNED NOT NULL,
    avoid_role           ENUM ('top', 'jungle', 'mid', 'adc', 'support'),
    PRIMARY KEY (summoner_id)
);
CREATE INDEX index_id ON lol_preferred_roles (player_id);

CREATE TABLE tournaments (
    `id`             VARCHAR(255) NOT NULL,
    `created`        DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `started`        TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `ended`          TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `allow_signups`  TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
    `public`         TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
    `round`          INT(4) NOT NULL DEFAULT 0,
	`type`           INT(4) UNSIGNED NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE competitors (
    `tournament_id`       VARCHAR(255) NOT NULL,
    `summoner_id`         VARCHAR(63) NOT NULL,
    `wins`                INT(5) NOT NULL DEFAULT 0,
    `mvps`                INT(5) NOT NULL DEFAULT 0,
	`playing`             TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
    PRIMARY KEY (`tournament_id`, `summoner_id`)
);

CREATE TABLE ranks (
    `tier`                VARCHAR(32) NOT NULL,
	`lp`                  INT(4) NOT NULL DEFAULT 0,
    `division`            INT(1) UNSIGNED NOT NULL DEFAULT 1,
    `summoner_id`         VARCHAR(63) NOT NULL,
    PRIMARY KEY (`summoner_id`)
);

CREATE TABLE discords (
    `tag`                 VARCHAR(255) NOT NULL,
    `summoner_id`         VARCHAR(63) NOT NULL,
    PRIMARY KEY (`summoner_id`)
);

CREATE TABLE auth_tokens (
    `token`           VARCHAR(255) NOT NULL,
    `created`         DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `expires`         DATETIME NOT NULL,
    `max_uses`        INT(10) NOT NULL DEFAULT -1
);

CREATE TABLE `games` (
    `game_id`      INT(10) NOT NULL AUTO_INCREMENT,
    `blue_team`    VARCHAR(1023) NOT NULL,
    `red_team`     VARCHAR(1023) NOT NULL,
    PRIMARY KEY (`game_id`)
);

CREATE TABLE `games_live` (
    `game_id`       INT(10),
    `tournament_id` VARCHAR(255) NOT NULL,
    `round`         INT(4) NOT NULL DEFAULT 1,
    `winner`        VARCHAR(255) NOT NULL DEFAULT '',
    `mvps`          VARCHAR(1023) NOT NULL DEFAULT '',
    PRIMARY KEY (`game_id`)
);
