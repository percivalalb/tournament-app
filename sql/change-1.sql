
ALTER TABLE ranks ADD COLUMN `lp` INT(4) NOT NULL DEFAULT 0;
ALTER TABLE ranks ADD COLUMN `division` INT(1) UNSIGNED NOT NULL DEFAULT 1;

UPDATE `ranks` SET `division` = 1 WHERE `extra` = `1` AND tier NOT IN('UNRANKED', 'MASTER', 'GRANDMASTER', 'CHALLENGER');
UPDATE `ranks` SET `division` = 2 WHERE `extra` = `2` AND tier NOT IN('UNRANKED', 'MASTER', 'GRANDMASTER', 'CHALLENGER');
UPDATE `ranks` SET `division` = 3 WHERE `extra` = `3` AND tier NOT IN('UNRANKED', 'MASTER', 'GRANDMASTER', 'CHALLENGER');
UPDATE `ranks` SET `division` = 4 WHERE `extra` = `4` AND tier NOT IN('UNRANKED', 'MASTER', 'GRANDMASTER', 'CHALLENGER');

ALTER TABLE ranks DROP COLUMN `extra`;