package com.alexbarter.lol.api;

import static com.alexbarter.lol.db.lol.tables.AuthTokens.*;
import static spark.Spark.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jooq.impl.DSL;

import com.alexbarter.lol.ConfigLoader;
import com.alexbarter.lol.MainTest;

import spark.Request;
import spark.Response;

public class AuthentiationAPI {

    private static final Map<String, String> programConfig = ConfigLoader.load("program");
    private static MessageDigest sha256Digest = getSHA256();
    private static MessageDigest md5Digest = getMD5();

    public static Object authenticate(Request req, Response res) {
        String password = req.queryParamOrDefault("password", "");

        if (isPasswordCorrect(password)) {
            SecureRandom random = new SecureRandom();
            byte[] randData = new byte[256];
            random.nextBytes(randData);
            byte[] sha256 = sha256Digest.digest(randData);
            String hash = bytesToHex(sha256);
            MainTest.dbh.execute(
              DSL.insertInto(AUTH_TOKENS, AUTH_TOKENS.TOKEN, AUTH_TOKENS.EXPIRES)
              .values(hash, Timestamp.valueOf(LocalDateTime.now().plusMinutes(180))));
            String redirect = req.queryParamOrDefault("redirect", "/lol/admin");
            try {
                redirect = URLDecoder.decode(redirect, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return APIResponse.create(req, res).set("token", hash).set("redirect", redirect).toString();
        }

        return APIResponse.create(req, res).setError("Incorrect password.");
    }

    public static boolean isPasswordCorrect(String password) {
        byte[] data = md5Digest.digest((password + programConfig.get("salt")).getBytes(StandardCharsets.UTF_8));
        String passwordHash = bytesToHex(data);
        return passwordHash.equals(programConfig.get("md5_hash"));
    }

    public static Pattern protocalHostname = Pattern.compile("(https?:\\/\\/.+?)\\/");

    public static void accessAllowed(Request req, Response res) {
        if (req.uri().equals("/lol/admin/login")) { return; }

        if (!isAuthenticated(req)) {
            res.type("application/json");

            String redirect = null;
            try {
                redirect = URLEncoder.encode(req.url(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Matcher match = protocalHostname.matcher(req.url());
            if (match.find()) {
                String baseURL = match.group(1);
                res.redirect(baseURL + "/lol/admin/login?redirect="+redirect);
                halt(401, APIUtils.createError("Access denied. "));
            }
        } else {
            setTokenExpiry(req, 60);
        }
    }

    public static String isAuthenticated(Request req, Response res) {
        return APIResponse.create(req, res).set("authenticated", isAuthenticated(req)).toString();
    }

    public static boolean isAuthenticated(Request req) {
        String auth = req.cookie("admin");
        if (auth == null) { return false; }

        return MainTest.dbh.fetchExists(DSL.select().from(AUTH_TOKENS).where(AUTH_TOKENS.TOKEN.eq(auth).and(AUTH_TOKENS.EXPIRES.greaterThan(DSL.currentTimestamp()))));
    }

    /**
     *
     * @param req
     * @param minutes
     */
    public static void setTokenExpiry(Request req, long minutes) {
        if (minutes <= 0) { return; }

        String auth = req.cookie("admin");
        if (auth == null) { return; }

        Timestamp newExpire = Timestamp.valueOf(LocalDateTime.now().plusMinutes(minutes));

        MainTest.dbh.execute(
          DSL.update(AUTH_TOKENS)
          .set(AUTH_TOKENS.EXPIRES, newExpire)
          .where(AUTH_TOKENS.TOKEN.eq(auth).and(AUTH_TOKENS.EXPIRES.lessThan(newExpire))));
    }

    private static MessageDigest getSHA256() {
        try {
            return MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            // Should never happen
            throw new RuntimeException(e.getMessage());
        }
    }

    private static MessageDigest getMD5() {
        try {
            return MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            // Should never happen
            throw new RuntimeException(e.getMessage());
        }
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
