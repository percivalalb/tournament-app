package com.alexbarter.lol.api;

import static com.alexbarter.lol.db.lol.tables.Accounts.*;
import static com.alexbarter.lol.db.lol.tables.Competitors.*;
import static com.alexbarter.lol.db.lol.tables.Discords.*;
import static com.alexbarter.lol.db.lol.tables.Games.*;
import static com.alexbarter.lol.db.lol.tables.GamesLive.*;
import static com.alexbarter.lol.db.lol.tables.Ranks.*;
import static com.alexbarter.lol.db.lol.tables.Roles.*;
import static com.alexbarter.lol.db.lol.tables.Tournaments.*;
import static org.jooq.impl.DSL.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.InsertValuesStep5;
import org.jooq.Record10;
import org.jooq.Record2;
import org.jooq.Result;
import org.jooq.types.UByte;
import org.jooq.types.UInteger;

import com.alexbarter.lib.Maps;
import com.alexbarter.lol.MainTest;
import com.alexbarter.lol.db.lol.enums.RolesAvoidRole;
import com.alexbarter.lol.db.lol.enums.RolesPrimaryRole;
import com.alexbarter.lol.db.lol.enums.RolesSecondaryRole;
import com.alexbarter.lol.db.lol.tables.records.GamesLiveRecord;
import com.alexbarter.lol.matchmaking.Game;
import com.alexbarter.lol.matchmaking.Matchmaking;
import com.alexbarter.lol.matchmaking.Player;
import com.alexbarter.lol.matchmaking.Rank;
import com.alexbarter.lol.matchmaking.Role;
import com.alexbarter.lol.matchmaking.Tier;
import com.alexbarter.lol.matchmaking.Tournament;

import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.api.endpoints.league.constant.LeagueQueue;
import net.rithms.riot.api.endpoints.league.dto.LeagueEntry;
import net.rithms.riot.api.endpoints.summoner.dto.Summoner;
import net.rithms.riot.constant.Platform;
import spark.Request;
import spark.Response;

public class TournamentAPI {


    public static Object add(Request req, Response res) {
        APIResponse api = APIResponse.create(req, res);
        String tournamentId = req.queryParams("tournament-id");

        if (tournamentId == null) {
            return api.setError("No tournament id given.");
        }

        int typeIndex = Integer.valueOf(req.queryParams("type"));

        MainTest.dbh.execute(
          insertInto(TOURNAMENTS, TOURNAMENTS.ID, TOURNAMENTS.TYPE)
          .values(tournamentId, UInteger.valueOf(typeIndex)));

        api.set("success", "Succesful created tournament.");
        return api.toString();
    }

    public static Object remove(Request req, Response res) {
        APIResponse api = APIResponse.create(req, res);

        String tournamentId = req.queryParams("tournamentId");

        if (tournamentId == null) {
            return api.setError("No tournament id given.");
        }

        MainTest.dbh.execute(
          delete(TOURNAMENTS)
          .where(TOURNAMENTS.ID.eq(tournamentId)));

        api.set("success", "Succesful removed tournament.");
        return api.toString();
    }

    public static Object get(Request req, Response res) {
        APIResponse json = APIResponse.create(req, res);
        json.set("tournaments",
                MainTest.dbh.getCtx(
                  select(TOURNAMENTS.ID, TOURNAMENTS.CREATED, TOURNAMENTS.TYPE,
                    select(count())
                    .from(COMPETITORS)
                    .where(COMPETITORS.TOURNAMENT_ID.eq(TOURNAMENTS.ID))
                    .asField().as("participants")
                )
                .from(TOURNAMENTS))
                .fetchMaps());
        return json.toString();
    }

    public static Object deregister(Request req, Response res) {
        APIResponse api = APIResponse.create(req, res);

        String tournamentId = req.queryParams("tournamentId");

        if (tournamentId == null) {
            return api.setError("No tournament id given.");
        }

        String summonerId = req.queryParams("summoner-id");

        if (summonerId == null) {
            return api.setError("No summoner id given.");
        }

        MainTest.dbh.execute(
          delete(COMPETITORS)
          .where(COMPETITORS.TOURNAMENT_ID.eq(tournamentId).and(COMPETITORS.SUMMONER_ID.eq(summonerId))));

        api.set("success", "Succesful removed player from tournament.");
        return api.toString();
    }

    public static void register(String tournamentId, String name, String accountId, String summonerId, String puuid, String discordTag, Rank rank, Role primaryRoles, Role secondaryRoles, boolean fill, Role avoidRole, boolean local) {
        MainTest.dbh.execute(
          insertInto(ACCOUNTS, ACCOUNTS.NAME, ACCOUNTS.ACCOUNT_ID, ACCOUNTS.SUMMONER_ID, ACCOUNTS.PUUID, ACCOUNTS.PLAYER_ID)
          .values(name, accountId, summonerId, puuid, 0)
          .onDuplicateKeyUpdate()
          .set(ACCOUNTS.NAME, name));

        MainTest.dbh.execute(insertInto(ROLES, ROLES.SUMMONER_ID, ROLES.PRIMARY_ROLE, ROLES.SECONDARY_ROLE, ROLES.FILL, ROLES.AVOID_ROLE)
          .values(summonerId, primaryRoles.get(), secondaryRoles.get2(), UByte.valueOf(fill ? 1 : 0), avoidRole != null ? avoidRole.get3() : null)
          .onDuplicateKeyUpdate()
          .set(ROLES.PRIMARY_ROLE, primaryRoles.get())
          .set(ROLES.SECONDARY_ROLE, secondaryRoles.get2())
          .set(ROLES.AVOID_ROLE, avoidRole != null && fill ? val(avoidRole.get3()) : val((RolesAvoidRole) null))
          .set(ROLES.FILL, UByte.valueOf(fill ? 1 : 0)));

        MainTest.dbh.execute(insertInto(DISCORDS, DISCORDS.SUMMONER_ID, DISCORDS.TAG)
          .values(summonerId, discordTag)
          .onDuplicateKeyUpdate()
          .set(DISCORDS.TAG, discordTag));

        MainTest.dbh.execute(
          insertInto(RANKS, RANKS.SUMMONER_ID, RANKS.TIER, RANKS.DIVISION, RANKS.LP)
          .values(summonerId, rank.getTier().name(), UInteger.valueOf(rank.getDivision()), rank.getLP())
          .onDuplicateKeyUpdate()
          .set(RANKS.TIER, rank.getTier().name())
          .set(RANKS.DIVISION, UInteger.valueOf(rank.getDivision()))
          .set(RANKS.LP, rank.getLP()));

        MainTest.dbh.execute(
          insertInto(COMPETITORS, COMPETITORS.SUMMONER_ID, COMPETITORS.TOURNAMENT_ID)
          .values(summonerId, tournamentId)
          .onDuplicateKeyIgnore());
    }

    public static boolean doesCompetitorExist(String summonerId, String tournamentId) {
        return MainTest.dbh.fetchExists(select()
                .from(COMPETITORS)
                .where(COMPETITORS.SUMMONER_ID.eq(summonerId).and(COMPETITORS.TOURNAMENT_ID.eq(tournamentId))));
    }

    public static Object togglePlaying(Request req, Response res) {
        APIResponse api = APIResponse.create(req, res);
        Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournamentId"));

        if (tournament == null) {
            return api.setError("Tournament does not exist.");
        }

        String summonerId = req.queryParams("summonerId");

        if (summonerId == null) {
            return api.setError("No summoner id given.");
        }

        boolean isPlaying =  MainTest.dbh.getCtx(
                select(COMPETITORS.PLAYING)
                .from(COMPETITORS)
                .where(COMPETITORS.TOURNAMENT_ID.eq(tournament.getId()).and(COMPETITORS.SUMMONER_ID.eq(summonerId)))).fetchOne().get(COMPETITORS.PLAYING).byteValue() != 0;

        MainTest.dbh.execute(
          update(COMPETITORS)
          .set(COMPETITORS.PLAYING, UByte.valueOf(isPlaying ? 0 : 1))
          .where(COMPETITORS.TOURNAMENT_ID.eq(tournament.getId()).and(COMPETITORS.SUMMONER_ID.eq(summonerId))));

        api.set("success", "Toggled player playing.");
        api.set("value", !isPlaying);
        return api.toString();
    }

    public static Object register(Request req, Response res) {

        Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournament-id"));

        if (tournament == null) {
            return APIResponse.create(req, res).setError("Tournament does not exist.");
        }

        if (!tournament.allowSignups()) {
            return APIResponse.create(req, res).setError("Tournament not currently accepting signups.");
        }

        Role primaryRole = Role.byName(req.queryParams("primary-role"));
        Role secondaryRole = Role.byName(req.queryParams("secondary-role"));
        Role avoidRole = Role.byName(req.queryParams("avoid-role"));
        boolean fill = "on".equalsIgnoreCase(req.queryParams("fill"));


        if (primaryRole == null || secondaryRole == null) {
            return APIResponse.create(req, res).setError("Invalid role given.");
        }

        if (primaryRole == secondaryRole) {
            return APIResponse.create(req, res).setError("Primary and secondary role cannot be the same.");
        }

        if (fill && (primaryRole == avoidRole || secondaryRole == avoidRole)) {
            return APIResponse.create(req, res).setError("Avoid role is the same as a primary or secondary role.");
        }

        String name = req.queryParams("summoner-name");
        Summoner summoner = null;
        Rank rank = null;
        try {
            Platform platform = Platform.getPlatformByName(req.queryParamOrDefault("platform", "euw"));
            summoner = MainTest.api.getSummonerByName(platform, name);
            if (doesCompetitorExist(summoner.getId(), tournament.getId())) {
                return APIResponse.create(req, res).setError("Already registered for tournament.");
            }

            if ("on".equalsIgnoreCase(req.queryParams("rank-manual"))) {
                try  {
                    rank = new Rank(req.queryParams("tier"), Integer.valueOf(req.queryParams("division")), Integer.valueOf(req.queryParams("lp")));
                } catch(Exception e) {
                    e.printStackTrace();
                    return APIResponse.create(req, res).setError("Unable to parse rank. " + e.getMessage());
                }
            } else {
                Map<String, LeagueEntry> leagueEntries =
                        MainTest.api.getLeagueEntriesBySummonerId(platform, summoner.getId())
                        .stream()
                        .collect(Collectors.toMap(LeagueEntry::getQueueType, x -> x));

                LeagueEntry soloLeague = leagueEntries.getOrDefault(LeagueQueue.RANKED_SOLO_5x5.toString(), null);

                if (soloLeague != null) {
                    System.out.println(soloLeague.toStringVerbosely(0));
                    rank = new Rank(soloLeague.getTier(), soloLeague.getRank(), soloLeague.getLeaguePoints());
                } else {
                    rank = new Rank(Tier.UNRANKED, 1);
                }
            }

            register(tournament.getId(), summoner.getName(), summoner.getAccountId(), summoner.getId(), summoner.getPuuid(), req.queryParams("discord-tag"), rank, primaryRole, secondaryRole, fill, avoidRole, true);
        } catch (RiotApiException e) {
            return APIResponse.create(req, res).setError(e.getMessage());
        }

        //res.cookie("currentTournamentId", tournamentId);
        //res.redirect("/lol/admin/tournament/"+tournamentId);
        return APIResponse.create(req, res).set("redirect", "/lol/tournament/"+tournament.getId()+"/leaderboard").toString();
    }

    public static Object reportResult(Request req, Response res) {
        APIResponse json = APIResponse.create(req, res);
        Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournament-id"));

        if (tournament == null) {
            return APIResponse.create(req, res).setError("Tournament does not exist.");
        }

        List<Integer> liveGames = MainTest.dbh.getCtx(
          select(GAMES_LIVE.GAME_ID)
          .from(GAMES_LIVE)
          .where(GAMES_LIVE.TOURNAMENT_ID.eq(tournament.getId())))
          .fetch(GAMES_LIVE.GAME_ID);

        for (int gameId : liveGames) {
            String winner = req.queryParams("game-"+gameId+"-winner");

            if (winner == null) { continue; }

            String blueSummonerId = req.queryParams("game-"+gameId+"-blue-mvp");
            String redSummonerId = req.queryParams("game-"+gameId+"-red-mvp");

            if (blueSummonerId == null || redSummonerId == null) {
                return json.setError("Both MVPs not provided for game " + gameId + ".");
            }

            Record2<String, String> record =  MainTest.dbh.getCtx(select(GAMES.BLUE_TEAM, GAMES.RED_TEAM).from(GAMES).where(GAMES.GAME_ID.eq(gameId))).fetchOne();
            String[] blueTeamPlayers = record.get(GAMES.BLUE_TEAM).split(",");
            String[] redTeamPlayers = record.get(GAMES.RED_TEAM).split(",");
            MainTest.dbh.execute(update(COMPETITORS)
               .set(COMPETITORS.WINS, COMPETITORS.WINS.add(1))
               .where(COMPETITORS.SUMMONER_ID.in(winner.equals("blue") ? blueTeamPlayers : redTeamPlayers).and(COMPETITORS.TOURNAMENT_ID.eq(tournament.getId()))));

            MainTest.dbh.execute(update(COMPETITORS)
              .set(COMPETITORS.MVPS, COMPETITORS.MVPS.add(1))
              .where(COMPETITORS.SUMMONER_ID.in(blueSummonerId, redSummonerId).and(COMPETITORS.TOURNAMENT_ID.eq(tournament.getId()))));

            MainTest.dbh.execute(
              update(GAMES_LIVE)
              .set(GAMES_LIVE.MVPS, String.join(",", blueSummonerId, redSummonerId))
              .set(GAMES_LIVE.WINNER, winner)
              .where(GAMES_LIVE.GAME_ID.eq(gameId)));

            Game game = Game.getOrLoadFromDB(gameId);
            game.mvps.add(blueSummonerId);
            game.mvps.add(redSummonerId);
            game.winner = winner;
        }

        Map<Integer, List<Game>> games = tournament.getAllGames().stream()
                .collect(Collectors.groupingBy(Game::getRound, TreeMap::new, Collectors.toList()));

        Maps params = Maps.of()
                .put("games_live", games)
                .put("tournament", tournament);
        json.set("html", APIUtils.render(params.get(), "/templates/game_live.vm"));
        return json.toString();
    }


    public static Object recreate(Request req, Response res) {
        Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournament-id"));

        if (tournament == null) {
            return APIResponse.create(req, res).setError("Tournament does not exist.");
        }

        String gameId = req.queryParams("game-id");

        if (gameId == null) {
            return APIResponse.create(req, res).setError("Game does not exist.");
        }

        Game game = Matchmaking.regenerateGame(Integer.valueOf(gameId));
        APIResponse response = APIResponse.create(req, res);
        response.set("games", Stream.of(game).map(Game::getHTML).collect(Collectors.toList()));
        return response.toString();
    }

    public static Object copyGame(Request req, Response res) {
        String gameId = req.queryParams("game-id");

        if (gameId == null) {
            return APIResponse.create(req, res).setError("Game does not exist.");
        }
        APIResponse response = APIResponse.create(req, res);
        Game game = Game.getOrLoadFromDB(Integer.valueOf(gameId));


        response.set("game", game.getASCIITable());
        return response.toString();
    }

   public static Object create(Request req, Response res) {
       Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournamentId"));

       if (tournament == null) {
           return APIResponse.create(req, res).setError("Tournament does not exist.");
       }

       Result<Record10<String, String, String, UInteger, Integer, String, RolesPrimaryRole, RolesSecondaryRole, RolesAvoidRole, UByte>> players = MainTest.dbh.getCtx(
                select(ACCOUNTS.NAME, ACCOUNTS.SUMMONER_ID, RANKS.TIER, RANKS.DIVISION, RANKS.LP, DISCORDS.TAG.as("discord_tag"), ROLES.PRIMARY_ROLE, ROLES.SECONDARY_ROLE, ROLES.AVOID_ROLE, ROLES.FILL)
                .from(COMPETITORS)
                  .leftOuterJoin(ACCOUNTS)
                  .on(ACCOUNTS.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
                  .leftOuterJoin(RANKS)
                  .on(RANKS.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
                  .leftOuterJoin(DISCORDS)
                  .on(DISCORDS.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
                  .leftOuterJoin(ROLES)
                  .on(ROLES.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
                .where(COMPETITORS.TOURNAMENT_ID.eq(tournament.getId()).and(COMPETITORS.PLAYING.ne(UByte.valueOf(0)))))
          .fetch();

        List<Player> playerPool = new ArrayList<>();

        for (Record10<String, String, String, UInteger, Integer, String, RolesPrimaryRole, RolesSecondaryRole, RolesAvoidRole, UByte> record : players) {
            Rank rank = new Rank(record.get("tier", String.class), record.get("division", UInteger.class).intValue(), record.get("lp", Integer.class));
            playerPool.add(new Player(record.get("name", String.class), record.get("summoner_id", String.class), rank,
                    Role.byDBEntry(record.get("primary_role")), Role.byDBEntry(record.get("secondary_role")), Role.byDBEntry(record.get("avoid_role")), record.get("fill", UByte.class).byteValue() == 1));
        }

        APIResponse response = APIResponse.create(req, res);
        List<Game> games = Matchmaking.generateGames(tournament, tournament.getRound() + 1, playerPool);

        response.set("games", games.stream().map(Game::getHTML).collect(Collectors.toList()));

        return response.toString();
    }

   public static Object competitors(Request req, Response res) {
       APIResponse json = APIResponse.create(req, res);
       Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournamentId"));

       if (tournament == null) {
           return APIResponse.create(req, res).setError("Tournament does not exist.");
       }

       List<Map<String, Object>> players = MainTest.dbh.getCtx(
         select(ACCOUNTS.NAME, ACCOUNTS.SUMMONER_ID, RANKS.TIER, RANKS.DIVISION, RANKS.LP, DISCORDS.TAG.as("discord_tag"), ROLES.PRIMARY_ROLE, ROLES.SECONDARY_ROLE, ROLES.AVOID_ROLE, ROLES.FILL, COMPETITORS.PLAYING)
         .from(COMPETITORS)
           .leftOuterJoin(ACCOUNTS)
           .on(ACCOUNTS.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
           .leftOuterJoin(RANKS)
           .on(RANKS.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
           .leftOuterJoin(DISCORDS)
           .on(DISCORDS.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
           .leftOuterJoin(ROLES)
           .on(ROLES.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
         .where(COMPETITORS.TOURNAMENT_ID.eq(tournament.getId())))
         .fetchMaps();

       for (Map<String, Object> playerData : players) {
           Rank rank = new Rank((String) playerData.get("tier"), ((UInteger) playerData.get("division")).intValue(), (Integer) playerData.get("lp"));
           playerData.put("elo", rank.getElo());
       }

       json.set("summoners", players);
       return json.toString();
   }

   public static Object leaderboard(Request req, Response res) {
       APIResponse json = APIResponse.create(req, res);
       Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournamentId"));

       if (tournament == null) {
           return APIResponse.create(req, res).setError("Tournament does not exist.");
       }
       List<Map<String, Object>> leaderboardData = MainTest.dbh.getCtx(
         select(COMPETITORS.SUMMONER_ID, ACCOUNTS.NAME, COMPETITORS.WINS, COMPETITORS.MVPS)
         .from(COMPETITORS)
         .leftOuterJoin(ACCOUNTS)
         .on(ACCOUNTS.SUMMONER_ID.eq(COMPETITORS.SUMMONER_ID))
         .where(COMPETITORS.TOURNAMENT_ID.eq(tournament.getId())))
         .fetchMaps();

       for (Map<String, Object> playerData : leaderboardData) {
           playerData.put("points", ((Integer) playerData.get("wins")).intValue() + ((Integer) playerData.get("mvps")).intValue());
       }

       json.set("leaderboard", leaderboardData);
       return json.toString();
   }

   public static Object lock(Request req, Response res) {
       APIResponse json = APIResponse.create(req, res);
       Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournamentId"));

       if (tournament == null) {
           return APIResponse.create(req, res).setError("Tournament does not exist.");
       }

       String roundStr = req.queryParamOrDefault("round", "1");
       int round = Integer.valueOf(roundStr);

       List<Integer> gameIds = Stream.of(req.queryParamsValues("games[]")).map(Integer::valueOf).collect(Collectors.toList());
       int currentRound = tournament.getRound();

       if (currentRound + 1 != round) {
           return json.setError("Looks like round " + round + " has already been made. Refresh the page!");
       }

       System.out.println(gameIds);
       tournament.nextRound();
       tournament.pushUpdate();

       InsertValuesStep5<GamesLiveRecord, Integer, String, Integer, String, String> stmt =
         insertInto(GAMES_LIVE, GAMES_LIVE.GAME_ID, GAMES_LIVE.TOURNAMENT_ID, GAMES_LIVE.ROUND, GAMES_LIVE.WINNER, GAMES_LIVE.MVPS);

       for (int gameId : gameIds) {
           stmt = stmt.values(gameId, tournament.getId(), round, "", "");
       }

       MainTest.dbh.execute(stmt);
       Game.get(gameIds).forEach(Game::lock);

       Map<Integer, List<Game>> games = tournament.getAllGames().stream()
               .collect(Collectors.groupingBy(Game::getRound, TreeMap::new, Collectors.toList()));

       Maps params = Maps.of()
               .put("games_live", games)
               .put("tournament", tournament);
       json.set("html", APIUtils.render(params.get(), "/templates/game_live.vm"));

       return json.toString();
   }

   public static Object toggleSignups(Request req, Response res) {

       APIResponse json = APIResponse.create(req, res);
       Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournamentId"));

       if (tournament == null) {
           return APIResponse.create(req, res).setError("Tournament does not exist.");
       }
       tournament.toggleSignups();
       tournament.pushUpdate();

       json.set("value", tournament.allowSignups());
       return json.toString();
   }

   public static Object start(Request req, Response res) {
       APIResponse json = APIResponse.create(req, res);
       Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournamentId"));

       if (tournament == null) {
           return APIResponse.create(req, res).setError("Tournament does not exist.");
       }

       tournament.setStarted(true);
       tournament.pushUpdate();

       return json.toString();
   }

   public static Object reset(Request req, Response res) {
       APIResponse json = APIResponse.create(req, res);
       Tournament tournament = Tournament.get(MainTest.dbh, req.queryParams("tournamentId"));

       if (tournament == null) {
           return APIResponse.create(req, res).setError("Tournament does not exist.");
       }

       tournament.reset();

       return json.toString();
   }
}
