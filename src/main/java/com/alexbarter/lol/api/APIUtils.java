package com.alexbarter.lol.api;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.jooq.types.UByte;
import org.jooq.types.UInteger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import spark.ModelAndView;
import spark.Request;
import spark.template.velocity.VelocityTemplateEngine;

public class APIUtils {

    public static Gson GSON;

    public static String createError(String errorMsg) {
        return "{\"error\":\""+errorMsg+"\"}";
    }
    static VelocityEngine velocityEngine ;
    static {
        Properties properties = new Properties();
        properties.setProperty("resource.loader", "class");
        properties.setProperty("classpath.resource.loader.cache", "true");
        properties.setProperty(
                "class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        velocityEngine = new org.apache.velocity.app.VelocityEngine(properties);
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(UByte.class, new JsonSerializer<UByte>() {
            @Override
            public JsonElement serialize(UByte src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.byteValue());
            }
        });

        builder.registerTypeAdapter(UInteger.class, new JsonSerializer<UInteger>() {
            @Override
            public JsonElement serialize(UInteger src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.longValue());
            }
        });
        GSON = builder.create();
    }

    public static String render(Map<String, Object> model, String templatePath) {
        return new VelocityTemplateEngine(velocityEngine).render(new ModelAndView(model, templatePath));
    }

    /**
     * Only allows one format of the following
     * ?key=2&key=3&key=5
     * ?key=2,3,5
     * ?key[]=2&key[]=3&key[]=5
     * @param key
     * @param req
     * @return
     */
    public static List<String> getArray(String key, Request req) {
        List<String> array = new ArrayList<>();
        String[] params = req.queryParamsValues(key);

        if (params != null) {
            for (String param : params) {
                for (String param2 : param.split(",")) {
                    array.add(param2);
                }
            }
        }

        params = req.queryParamsValues(key+"[]");

        if (params != null) {
            for (String param : params) {
                array.add(param);
            }
        }

        return array;
    }
}
