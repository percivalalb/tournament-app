/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019  Alex Barter

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

package com.alexbarter.lol.api;

import java.util.HashMap;
import java.util.Map;

import spark.Request;
import spark.Response;

public class APIResponse {

    private Request req;
    private Response res;
    private Map<String, Object> result = new HashMap<>();
    private String error;

    private APIResponse(Request req, Response res) {
        this.req = req;
        this.res = res;
    }

    public APIResponse set(String response, Object object) {
        this.result.put(response, object);
        return this;
    }

    public String setError(String msg) {
        this.error = msg;
        this.res.status(400);
        this.result.clear();
        return this.toString();
    }

    @Override
    public String toString() {
        this.res.type("application/json");

        if (this.error != null) {
            this.res.status(400);
            return APIUtils.createError(this.error);
        }

        return APIUtils.GSON.toJson(this.result);
    }

    public static APIResponse create(Request req, Response res) {
        return new APIResponse(req, res);
    }
}
