package com.alexbarter.lol.matchmaking;

import java.util.HashMap;
import java.util.Map;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.types.UByte;

import com.alexbarter.lol.DatabaseConnection;

public abstract class DBObject<R extends Record> {

    public final DatabaseConnection dbh;
    public final Table<R> table;
    private Map<TableField<R, ?>, Object> changedFields;

    public DBObject(DatabaseConnection dbh, Table<R> table) {
        this.dbh = dbh;
        this.table = table;
        this.changedFields = new HashMap<>();
    }

    public <T> void markDirty(TableField<R, T> field, T value) {
        this.changedFields.put(field, value);
    }

    public <T> void markDirty(TableField<R, UByte> field, boolean value) {
        this.markDirty(field, UByte.valueOf(value ? 1 : 0));
    }

    public boolean pushUpdate() {
        if (this.changedFields.isEmpty()) {
            return true;
        }

        int update = this.dbh.execute(DSL.update(this.table).set(this.changedFields).where(this.getCondition()));
        return update == this.changedFields.size();
    }

    public abstract Condition getCondition();
}
