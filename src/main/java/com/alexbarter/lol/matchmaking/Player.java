package com.alexbarter.lol.matchmaking;

import java.util.Objects;

import javax.annotation.Nullable;

public class Player {

    private String name;
    private String summonerId;
    private Rank rank;
    private Role primaryRole;
    private Role secondaryRole;
    private Role avoidRole;
    private boolean fill;

    public Player(String name, String summonerId, Rank rank, Role primaryRole, Role secondaryRole, Role avoidRole,
            boolean fill) {
        super();
        this.name = name;
        this.summonerId = summonerId;
        this.rank = rank;
        this.primaryRole = primaryRole;
        this.secondaryRole = secondaryRole;
        this.avoidRole = avoidRole;
        this.fill = fill;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.summonerId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Player other = (Player) obj;

        return Objects.equals(this.summonerId, other.summonerId);
    }

    public String getName() {
        return this.name;
    }

    public String getSummonerId() {
        return this.summonerId;
    }

    public Rank getRank() {
        return this.rank;
    }

    public Role getPrimaryRole() {
        return this.primaryRole;
    }

    public Role getSecondaryRole() {
        return this.secondaryRole;
    }

    @Nullable
    public Role getAvoidRole() {
        return this.avoidRole;
    }

    public boolean canFill() {
        return this.fill;
    }

    public RoleMatch getRoleMatch(Role role) {
        if (role.canPlay(this.primaryRole)) {
            return RoleMatch.PRIMARY;
        } else if (role.canPlay(this.secondaryRole)) {
            return RoleMatch.SECONDARY;
        } else if (role.canPlay(this.avoidRole)) {
            return RoleMatch.AVOID;
        }  else if (this.canFill()) {
            return RoleMatch.FILL;
        } else {
            return RoleMatch.AUTOFILL;
        }
    }

    public String getColour(Role role) {
        if (role.canPlay(this.primaryRole)) {
            return "#007bff";
        } else if (role.canPlay(this.secondaryRole)) {
            return "#28a745";
        } else if (role.canPlay(this.avoidRole)) {
            return "#dc3545";
        }  else if (this.canFill()) {
            return "#bfff00";
        } else {
            return "#ffc107";
        }
    }

    @Override
    public String toString() {
        return "Player [name=" + name + ", summonerId=" + summonerId + ", rank=" + rank + ", primaryRole=" + primaryRole
                + ", secondaryRole=" + secondaryRole + ", avoidRole=" + avoidRole + ", fill=" + fill + "]";
    }

}
