package com.alexbarter.lol.matchmaking;

import static com.alexbarter.lol.db.lol.tables.Games.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import org.jooq.impl.DSL;

import com.alexbarter.lib.CollectionUtil;
import com.alexbarter.lib.Pair;
import com.alexbarter.lol.MainTest;
import com.alexbarter.lol.db.lol.tables.records.GamesRecord;

public class Matchmaking {

    private static Random rand = new Random();

    public static List<Game> generateGames(Tournament tournament, int round, List<Player> playerPool) {

        System.out.println("Creating games for id=" + tournament.getId());
        ArrayList<Game> games = new ArrayList<>();

        int numPlayers = playerPool.size();

        while(numPlayers > 0) {
            int temp = Math.min(numPlayers, tournament.getType() == Tournament.Type.ARAM ? 2 : 10);
            games.add(new Game(tournament, round, temp / 2));
            numPlayers -= temp;
        }

        System.out.println("Trying to create " + games.size() + " games.");

        // Pick a random game and choose a role to add someone too
        while (!CollectionUtil.allMatch(games, Game::complete)) {
            Game game = games.get(rand.nextInt(games.size()));
            if (game.complete()) continue;

            Team team = game.getRandomIncompleteTeam();

            // Get roles missing from team
            List<Role> remainingRoles = new ArrayList<>(team.getRemainingRoles());

            // Pick a role from the choices
            Role role = remainingRoles.get(rand.nextInt(remainingRoles.size()));

            // Choose a player for the role
            Pair<Player, RoleMatch> player = pickPlayerForRole(playerPool, role);
            playerPool.remove(player.getLeft());
            team.setPlayer(role, player.getLeft());
        }

        for (Game game : games) {
            game.gameId = saveGame(game);
            Game.CACHE.put(game.gameId, game);
        }

        return games;
    }

    public static Pair<Player, RoleMatch> pickPlayerForRole(List<Player> players, Role role) {
        //System.out.println("Players in: " + players.size());
        List<Player> actualPlayers = players.stream().filter(p -> p.getAvoidRole() != role).collect(Collectors.toList());
        //System.out.println("Players not wanting to play role: " + (players.size() - actualPlayers.size()));

        if (actualPlayers.isEmpty()) {
            //System.out.println("Having to put someone on avoid role :/");
            return Pair.of(players.get(rand.nextInt(players.size())), RoleMatch.AVOID);
        } else {
            List<Player> primaryPlayers = actualPlayers.stream().filter(p -> p.getPrimaryRole().canPlay(role)).collect(Collectors.toList());
            List<Player> secondaryPlayers = actualPlayers.stream().filter(p -> p.getSecondaryRole().canPlay(role)).collect(Collectors.toList());

            // If a player is in primary list make sure they are not also in secondary list
            secondaryPlayers.removeAll(primaryPlayers);
            if (!primaryPlayers.isEmpty()) {
                return Pair.of(primaryPlayers.get(rand.nextInt(primaryPlayers.size())), RoleMatch.PRIMARY);
            }

            if (!secondaryPlayers.isEmpty()) {
                return Pair.of(secondaryPlayers.get(rand.nextInt(secondaryPlayers.size())), RoleMatch.SECONDARY);
            }

           // System.out.println("Someone getting autofilled.");
            List<Player> fillPlayers = actualPlayers.stream().filter(Player::canFill).collect(Collectors.toList());
            if (!fillPlayers.isEmpty()) {
                return Pair.of(fillPlayers.get(rand.nextInt(fillPlayers.size())), RoleMatch.FILL);
            }

            return Pair.of(actualPlayers.get(rand.nextInt(actualPlayers.size())), RoleMatch.AUTOFILL);
        }
    }

    public static Optional<Game> getPreviousGame(Tournament tournament, Player player, int currentRound) {
        return tournament
                .getGamesForRound(currentRound - 1).stream()
                .filter(g -> g.isPlaying(player))
                .findFirst();
    }

    /**
     * Saves game to database and returns the id
     * @param game The gameId saved to the database
     * @return The gameId saved to the database
     */
    public static int saveGame(Game game) {
        StringJoiner blueTeam = new StringJoiner(",");
        StringJoiner redTeam = new StringJoiner(",");
        for (Role role : game.getRoles()) {
            for (Pair<Player, Player> pair : game.getPlayerPair(role)) {
                blueTeam.add(pair.getLeft().getSummonerId());
                redTeam.add(pair.getRight().getSummonerId());
            }
        }

        GamesRecord gameIdField = MainTest.dbh.getCtx(
           DSL.insertInto(GAMES, GAMES.BLUE_TEAM, GAMES.RED_TEAM)
          .values(blueTeam.toString(), redTeam.toString())
          .returning(GAMES.GAME_ID))
          .fetchOne();

        return gameIdField.get(GAMES.GAME_ID);
    }

    public static Game regenerateGame(int gameId) {
        return regenerateGame(Game.getOrLoadFromDB(gameId));
    }

    public static Game regenerateGame(Game prevGame) {
        List<Player> playerList = prevGame.getPlayerList();
        Game game = generateGames(prevGame.tournament, prevGame.round, playerList).get(0);
        return game;
    }
}
