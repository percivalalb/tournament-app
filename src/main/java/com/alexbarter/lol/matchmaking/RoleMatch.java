package com.alexbarter.lol.matchmaking;

public enum RoleMatch {

    PRIMARY,
    SECONDARY,
    FILL,
    AUTOFILL,
    AVOID;

    public boolean bad() {
        return this == FILL || this == AUTOFILL || this == AVOID;
    }
}
