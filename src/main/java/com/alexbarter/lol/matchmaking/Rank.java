package com.alexbarter.lol.matchmaking;

import java.util.Arrays;

public class Rank {

    private Tier tier;
    private int division;
    /** Stores either the division or the LP for master+ **/
    private int lp;

    private static String[] DIVISION_STR = {"I", "II", "III", "IV"};

    public Rank(String tierStr, String division, int leaguePoints) {
        this(tierStr, getDivisionFromString(division), leaguePoints);
    }

    public Rank(String tierStr, int division, int leaguePoints) {
        this.tier = Tier.byName(tierStr);
        this.division = division;
        this.lp = leaguePoints;
    }

    public Rank(String tier, int division) {
        this(Tier.byName(tier), division);
    }

    public Rank(Tier tier, int division) {
        if (tier.hasDivisions() && (1 > division || division > 4)) {
            throw new IllegalArgumentException("Invalid division given: " + division);
        }

        this.tier = tier;
        this.division = division;
        this.lp = 0;
    }

    private static int getDivisionFromString(String division) {
        return Arrays.asList(DIVISION_STR).indexOf(division) + 1;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.tier.name()).append(" ");

        if (this.tier.hasDivisions()) {
            builder.append(DIVISION_STR[this.division - 1]);
        } else if (this.tier != Tier.UNRANKED) {
            builder.append(this.lp);
            builder.append("LP");
        } else {
            builder.append("Unranked");
        }

        return builder.toString();
    }

    public Tier getTier() {
        return this.tier;
    }

    public int getDivision() {
        return this.division;
    }

    public int getLP() {
        return this.lp;
    }

    public int getElo() {
        int elo = this.tier.quantify();
        if (this.tier.hasDivisions()) {
            elo += (5 - this.division) * 100;
        }

        return elo + this.getLP();
    }
}
