package com.alexbarter.lol.matchmaking;

import java.util.NoSuchElementException;

public enum Tier {

    UNRANKED(false, 400),
    IRON(400),
    BRONZE(800),
    SILVER(1200),
    GOLD(1600),
    PLATINUM(2000),
    DIAMOND(2400),
    MASTER(false, 2800),
    GRANDMASTER(false, 2800),
    CHALLENGER(false, 3000);

    private boolean hasDivisions;
    private int elo;

    Tier(int elo) {
        this(true, elo);
    }

    Tier(boolean divisions, int elo) {
        this.hasDivisions = divisions;
        this.elo = elo;
    }

    public boolean hasDivisions() {
        return this.hasDivisions;
    }

    public int quantify() {
        return this.elo;
    }

    public static Tier byName(String name) {
        for (Tier tier : Tier.values()) {
            if (tier.name().equalsIgnoreCase(name)) {
                return tier;
            }
        }

        // Some commonly used short hands
        if ("plat".equalsIgnoreCase(name)) {
            return PLATINUM;
        }

        throw new NoSuchElementException("Unknown tier name: " + name);
    }

}
