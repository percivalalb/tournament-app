package com.alexbarter.lol.matchmaking;

import static com.alexbarter.lol.db.lol.tables.Accounts.*;
import static com.alexbarter.lol.db.lol.tables.Competitors.*;
import static com.alexbarter.lol.db.lol.tables.Discords.*;
import static com.alexbarter.lol.db.lol.tables.Games.*;
import static com.alexbarter.lol.db.lol.tables.GamesLive.*;
import static com.alexbarter.lol.db.lol.tables.Ranks.*;
import static com.alexbarter.lol.db.lol.tables.Roles.*;
import static org.jooq.impl.DSL.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.jooq.Record10;
import org.jooq.Record6;
import org.jooq.Result;
import org.jooq.types.UByte;
import org.jooq.types.UInteger;

import com.alexbarter.lib.Maps;
import com.alexbarter.lib.Pair;
import com.alexbarter.lol.MainTest;
import com.alexbarter.lol.api.APIUtils;
import com.alexbarter.lol.db.lol.enums.RolesAvoidRole;
import com.alexbarter.lol.db.lol.enums.RolesPrimaryRole;
import com.alexbarter.lol.db.lol.enums.RolesSecondaryRole;

public class Game implements IGame {

    public Tournament tournament;
    public int round;
    public final Team blue;
    public final Team red;
    public int gameId;
    public String winner;
    private Random rand = new Random();
    public List<String> mvps;
    public boolean locked;

    public Game(Tournament tournament, int round, int numPlayers) {
        this.tournament = tournament;
        this.round = round;
        this.blue = new Team("blue", numPlayers);
        this.red = new Team("red", numPlayers);
        this.mvps = new ArrayList<>();
    }

    public Game lock() {
        this.locked = true;
        return this;
    }

    public int getRound() {
        return this.round;
    }

    public Tournament getTournament() {
        return this.tournament;
    }

    public List<Role> getRemainingRolesBlue() {
        return this.blue.getRemainingRoles();
    }

    public List<Role> getRemainingRolesRed() {
        return this.red.getRemainingRoles();
    }

    public boolean setPlayerBlue(Role role, Player player) {
        return this.blue.setPlayer(role, player);
    }

    public boolean setPlayerRed(Role role, Player player) {
        return this.red.setPlayer(role, player);
    }

    public int getEloBlue() {
        return this.blue.getElo();
    }

    public int getEloRed() {
        return this.red.getElo();
    }

    public boolean complete() {
        return this.blue.complete() && this.red.complete();
    }

    public List<Player> getPlayerList() {
        List<Player> playerList = new ArrayList<>(this.blue.numPlayers + this.red.numPlayers);
        playerList.addAll(this.blue.getPlayerList());
        playerList.addAll(this.red.getPlayerList());
        return playerList;
    }


    public boolean isPlaying(Player player) {
        return this.blue.isPlaying(player) || this.red.isPlaying(player);
    }

    public List<Pair<Player, Player>> getPlayerPair(Role role) {
        List<Player> blue = this.blue.getPlayers(role);
        List<Player> red = this.red.getPlayers(role);
        List<Pair<Player, Player>> playerList = new ArrayList<>(blue.size() * 2);
        for (int i = 0; i < blue.size(); i++) {
            playerList.add(Pair.of(blue.get(i), red.get(i)));
        }
        return playerList;
    }

    public Team getRandomIncompleteTeam() {
        if ((this.rand.nextBoolean() || this.red.complete()) && !this.blue.complete()) {
            return this.blue;
        } else {
            return this.red;
        }
    }

    public String getHTML() {
        return APIUtils.render(Maps.of().put("game", this).put("blue", this.blue).put("red", this.red).get(), "templates/game.vm");
    }

    public String getDetails(Player player, Role role) {
        StringBuilder builder = new StringBuilder();

        if (!this.locked) {

            Optional<Game> lastGame = Matchmaking.getPreviousGame(this.tournament, player, this.round);

            lastGame.ifPresent(g -> {
                Team lastTeam = g.getTeam(player);
                Team lastEnemyTeam = g.getOtherTeam(lastTeam);
                Role lastRole = lastTeam.getRole(player);
                RoleMatch lastMatch = player.getRoleMatch(lastRole);


                RoleMatch match = player.getRoleMatch(role);
                if (match != RoleMatch.PRIMARY) {
                    builder.append("<span title\"Role type played last game.\" class=\"badge badge-danger\">"+lastMatch+"</span>");
                }

                Collection<Player> players = g.getPlayerList();
                long numSamePlayers = this.getPlayerList().stream().filter(players::contains).count() - 1L;

                if (numSamePlayers >= 6)
                builder.append("<span title=\"Number of matching players compared to last game.\" class=\"badge badge-danger\">"+numSamePlayers+" IN GAME</span>");
                players = lastTeam.getPlayerList();
                numSamePlayers = this.getTeam(player).getPlayerList().stream().filter(players::contains).count() - 1L;

                if (numSamePlayers >= 3)
                builder.append("<span title=\"Number of matching players in team compared to last game.\" class=\"badge badge-danger\">"+numSamePlayers+" IN TEAM</span>");

                if (lastRole != Role.ANY && role != Role.ANY && lastRole == role) {
                    Team team = this.getTeam(player);
                    if (lastEnemyTeam.getPlayer(role).equals(this.getOtherTeam(team).getPlayer(role))) {
                        builder.append("<span title=\"Same lane opponent as last game.\" class=\"badge badge-danger\">MATCHUP</span>");
                    }
                }
            });
        }

        return builder.toString();
    }

    /**
     * Assumes the player is in the game
     * @param player
     * @return
     */
    private Team getTeam(Player player) {
        return this.blue.isPlaying(player) ? this.blue : (this.red.isPlaying(player) ? this.red : null);
    }

    public Team getOtherTeam(Team team) {
        return this.blue == team ? this.red : (this.red == team ? this.blue : null);
    }

    @Override
    public String toString() {
        return "Game [id="+this.gameId+"]";
    }

    public boolean hasWinner() {
        return this.winner != null && !this.winner.isEmpty();
    }

    public boolean isWinner(String winner) {
        return this.winner != null && this.winner.equalsIgnoreCase(winner);
    }

    public boolean isMvp(String mvp) {
        return this.mvps.contains(mvp);
    }

    public Team getBlueTeam() {
        return this.blue;
    }

    public Team getRedTeam() {
        return this.red;
    }

    public Set<Role> getRoles() {
        switch (this.blue.numPlayers) {
        default: return EnumSet.of(Role.TOP, Role.JUNGLE, Role.MID, Role.ADC, Role.SUPPORT);
        case 4: return EnumSet.of(Role.ANY);
        case 3: return EnumSet.of(Role.ANY);
        case 2: return EnumSet.of(Role.ANY);
        case 1: return EnumSet.of(Role.ANY);
        }
    }

    public int getID() {
        return this.gameId;
    }

    public String getASCIITable() {
        StringBuilder builder = new StringBuilder(18 * 9 + 6);
        builder.append("```").append(System.lineSeparator());

        String leftAlignFormat = "| %-20s | %-20s |";

        builder.append("+----------------------+----------------------+").append(System.lineSeparator());
        builder.append("| Blue Team            | Red Team             |").append(System.lineSeparator());
        builder.append("+----------------------+----------------------+").append(System.lineSeparator());
        for (Role role : this.getRoles()) {
            for (Pair<Player, Player> pair : this.getPlayerPair(role)) {
                builder.append(
                        String.format(leftAlignFormat,
                                pair.getLeft().getName(),
                                pair.getRight().getName()
                        )
                ).append(System.lineSeparator());
            }
        }
        builder.append("+----------------------+----------------------+").append(System.lineSeparator());

        builder.append("```");
        return builder.toString();
    }

    @Override
    public int getNumPlayersPerTeam() {
        return 5;
    }

    public static final Map<Integer, Game> CACHE = new HashMap<>();

    @Nullable
    public static Game get(int gameId) {
        return getOrLoadFromDB(gameId);
    }

    public static List<Game> get(Iterable<Integer> gameIds) {
        List<Game> games = new ArrayList<>();
        for (int gameId : gameIds) {
            games.add(getOrLoadFromDB(gameId));
        }
        return games;
    }

    public static Game getOrLoadFromDB(int gameId) {
        return Game.CACHE.computeIfAbsent(gameId, Game::loadFromDB);
    }

    public static Game loadFromDB(int gameId) {
        Record6<String, String, String, String, Integer, String> gameData = MainTest.dbh.getCtx(
          select(GAMES.BLUE_TEAM, GAMES.RED_TEAM, GAMES_LIVE.WINNER, GAMES_LIVE.TOURNAMENT_ID, GAMES_LIVE.ROUND, GAMES_LIVE.MVPS)
          .from(GAMES)
          .leftOuterJoin(GAMES_LIVE)
          .on(GAMES.GAME_ID.eq(GAMES_LIVE.GAME_ID))
          .where(GAMES.GAME_ID.eq(gameId)))
          .fetchOne();

        String[] bluePlayers = gameData.get(GAMES.BLUE_TEAM).split(",");
        String[] redPlayers = gameData.get(GAMES.RED_TEAM).split(",");
        String[] mvpPlayers = gameData.get(GAMES_LIVE.MVPS).split(",");

        Tournament tournament = Tournament.get(MainTest.dbh, gameData.get(GAMES_LIVE.TOURNAMENT_ID));
        Game game = new Game(tournament, gameData.get(GAMES_LIVE.ROUND), bluePlayers.length);
        game.gameId = gameId;
        game.winner = gameData.get(GAMES_LIVE.WINNER);
        game.lock();


        Result<Record10<String, String, String, UInteger, Integer, String, RolesPrimaryRole, RolesSecondaryRole, RolesAvoidRole, UByte>> players = MainTest.dbh.getCtx(
           select(ACCOUNTS.NAME, ACCOUNTS.SUMMONER_ID, RANKS.TIER, RANKS.DIVISION, RANKS.LP, DISCORDS.TAG.as("discord_tag"), ROLES.PRIMARY_ROLE, ROLES.SECONDARY_ROLE, ROLES.AVOID_ROLE, ROLES.FILL)
          .from(ACCOUNTS)
            .leftJoin(COMPETITORS)
            .on(COMPETITORS.SUMMONER_ID.eq(ACCOUNTS.SUMMONER_ID))
            .leftJoin(RANKS)
            .on(RANKS.SUMMONER_ID.eq(ACCOUNTS.SUMMONER_ID))
            .leftJoin(DISCORDS)
            .on(DISCORDS.SUMMONER_ID.eq(ACCOUNTS.SUMMONER_ID))
            .leftJoin(ROLES)
            .on(ROLES.SUMMONER_ID.eq(ACCOUNTS.SUMMONER_ID)))
            .where(ACCOUNTS.SUMMONER_ID.in(bluePlayers).or(ACCOUNTS.SUMMONER_ID.in(redPlayers)))
          .fetch();
        Map<String, Player> playerPool = new HashMap<>();
        for (Record10<String, String, String, UInteger, Integer, String, RolesPrimaryRole, RolesSecondaryRole, RolesAvoidRole, UByte> record : players) {


            Rank rank = new Rank(record.get(RANKS.TIER), record.get(RANKS.DIVISION).intValue(), record.get(RANKS.LP));
            String summonerId = record.get(ACCOUNTS.SUMMONER_ID);
            playerPool.put(summonerId, new Player(record.get(ACCOUNTS.NAME), summonerId, rank,
                  Role.byDBEntry(record.get(ROLES.PRIMARY_ROLE)), Role.byDBEntry(record.get(ROLES.SECONDARY_ROLE)), Role.byDBEntry(record.get(ROLES.AVOID_ROLE)), record.get(ROLES.FILL).byteValue() != 0));
        }

        int i = 0;
        for (Role role : game.blue.getRoles()) {
            game.setPlayerBlue(role, playerPool.get(bluePlayers[i]));
            game.setPlayerRed(role, playerPool.get(redPlayers[i]));
            i++;
        }

        for (String mvps : mvpPlayers) {
            game.mvps.add(mvps);
        }

        System.out.println("Loaded game " + gameId + " from database.");

        return game;
    }
}
