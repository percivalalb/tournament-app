package com.alexbarter.lol.matchmaking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.alexbarter.lib.Pair;

public class Team {

    public final Set<Pair<Player, Role>> players;
    public final int numPlayers;
    public String colour;

    public Team(String colour, int numPlayers) {
        this.players = new HashSet<>();
        this.numPlayers = numPlayers;
        this.colour = colour;
    }

    public List<Role> getRemainingRoles() {
        List<Role> roles = this.getRoles();
        this.players.forEach(pair -> roles.remove(pair.getRight()));
        return roles;
    }

    public List<Role> getRoles() {
        switch (this.numPlayers) {
        default: return new ArrayList<>(Arrays.asList(Role.TOP, Role.JUNGLE, Role.MID, Role.ADC, Role.SUPPORT));
        case 4: return new ArrayList<>(Arrays.asList(Role.ANY, Role.ANY, Role.ANY, Role.ANY));
        case 3: return new ArrayList<>(Arrays.asList(Role.ANY, Role.ANY, Role.ANY));
        case 2: return new ArrayList<>(Arrays.asList(Role.ANY, Role.ANY));
        case 1: return new ArrayList<>(Arrays.asList(Role.ANY));
        }
    }

    public boolean setPlayer(Role role, Player player) {
        Objects.requireNonNull(player, "Player cannot be null.");
        this.players.add(Pair.of(player, role));
        return true;
    }

    public Collection<Player> getPlayerList() {
        return this.players.stream().map(Pair::getLeft).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Team [getEloBlue()=" + getElo() + ", complete()=" + complete() + "]";
    }

    public int getElo() {
        return this.players.stream().collect(Collectors.summingInt(p -> p.getLeft().getRank().getElo()));
    }

    public boolean complete() {
        return this.players.size() == this.numPlayers;
    }

    public Player getPlayer(Role role) {
        return this.players.stream().filter(pair -> pair.getRight().canPlay(role)).map(Pair::getLeft).findFirst().orElse(null);
    }

    public List<Player> getPlayers(Role role) {
        return this.players.stream().filter(pair -> pair.getRight().canPlay(role)).map(Pair::getLeft).collect(Collectors.toList());
    }

    public boolean isPlaying(Player player) {
        return this.players.stream().anyMatch(pair -> pair.getLeft().equals(player));
    }

    public Role getRole(Player player) {
        return this.players.stream().filter(pair -> pair.getLeft().equals(player)).findFirst().get().getRight();
    }
}
