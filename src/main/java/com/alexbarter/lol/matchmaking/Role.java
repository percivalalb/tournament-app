package com.alexbarter.lol.matchmaking;

import com.alexbarter.lol.db.lol.enums.RolesAvoidRole;
import com.alexbarter.lol.db.lol.enums.RolesPrimaryRole;
import com.alexbarter.lol.db.lol.enums.RolesSecondaryRole;

public enum Role {

    TOP(RolesPrimaryRole.top, RolesSecondaryRole.top, RolesAvoidRole.top),
    JUNGLE(RolesPrimaryRole.jungle, RolesSecondaryRole.jungle, RolesAvoidRole.jungle),
    MID(RolesPrimaryRole.mid, RolesSecondaryRole.mid, RolesAvoidRole.mid),
    ADC(RolesPrimaryRole.adc, RolesSecondaryRole.adc, RolesAvoidRole.adc),
    SUPPORT(RolesPrimaryRole.support, RolesSecondaryRole.support, RolesAvoidRole.support),
    ANY(null, null, null);

    private RolesPrimaryRole dbRole;
    private RolesSecondaryRole dbRole2;
    private RolesAvoidRole dbRole3;

    Role(RolesPrimaryRole dbRole, RolesSecondaryRole dbRole2, RolesAvoidRole dbRole3) {
        this.dbRole = dbRole;
        this.dbRole2 = dbRole2;
        this.dbRole3 = dbRole3;
    }

    public boolean canPlay(Role other) {
        return this == ANY || other == ANY || this == other;
    }

    public static Role byName(String name) {
        if (name == null) { return null; }

        for (Role role : Role.values()) {
            if (role.name().equalsIgnoreCase(name)) {
                return role;
            }
        }

        return null;
    }

    public static Role byDBEntry(Object dbEntry) {
        String name = null;

        if (dbEntry instanceof RolesPrimaryRole) {
            name = ((RolesPrimaryRole) dbEntry).getLiteral();
        } else if (dbEntry instanceof RolesSecondaryRole) {
            name = ((RolesSecondaryRole) dbEntry).getLiteral();
        } else if (dbEntry instanceof RolesAvoidRole) {
            name = ((RolesAvoidRole) dbEntry).getLiteral();
        }

        return byName(name);
    }

    public RolesPrimaryRole get() {
        return this.dbRole;
    }

    public RolesSecondaryRole get2() {
        return this.dbRole2;
    }

    public RolesAvoidRole get3() {
        return this.dbRole3;
    }
}
