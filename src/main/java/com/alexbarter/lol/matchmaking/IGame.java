package com.alexbarter.lol.matchmaking;

public interface IGame {

    default int getNumPlayers() {
        return this.getNumPlayersPerTeam() * 2;
    }

    int getNumPlayersPerTeam();
}
