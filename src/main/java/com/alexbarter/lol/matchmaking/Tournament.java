package com.alexbarter.lol.matchmaking;

import static com.alexbarter.lol.db.lol.tables.Competitors.*;
import static com.alexbarter.lol.db.lol.tables.GamesLive.*;
import static com.alexbarter.lol.db.lol.tables.Tournaments.*;
import static org.jooq.impl.DSL.*;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.jooq.Condition;
import org.jooq.Record;

import com.alexbarter.lol.DatabaseConnection;
import com.alexbarter.lol.db.lol.tables.records.TournamentsRecord;

public class Tournament extends DBObject<TournamentsRecord> {

    private final String id;
    private Type type;
    private LocalDateTime created;
    private boolean started;
    private boolean ended;
    private boolean allowSignups;
    private boolean isPublic;
    private int round;

    private Tournament(DatabaseConnection dbh, String id) {
        super(dbh, TOURNAMENTS);
        Record record = dbh.getCtx(select().from(TOURNAMENTS).where(TOURNAMENTS.ID.eq(id))).fetchOne();
        this.id = id;
        this.type =  Type.byIndex(record.get(TOURNAMENTS.TYPE));
        this.created = record.get(TOURNAMENTS.CREATED).toLocalDateTime();
        this.started = record.get(TOURNAMENTS.STARTED).byteValue() != 0;
        this.ended = record.get(TOURNAMENTS.ENDED).byteValue() != 0;
        this.allowSignups = record.get(TOURNAMENTS.ALLOW_SIGNUPS).byteValue() != 0;
        this.isPublic = record.get(TOURNAMENTS.ALLOW_SIGNUPS).byteValue() != 0;
        this.round = record.get(TOURNAMENTS.ROUND);
    }

    public String getId() {
        return this.id;
    }

    public Type getType() {
        return this.type;
    }

    public LocalDateTime getCreated() {
        return this.created;
    }

    public boolean hasStarted() {
        return this.started;
    }

    public boolean hasEnded() {
        return this.ended;
    }

    public boolean allowSignups() {
        return this.allowSignups;
    }

    public boolean isPublic() {
        return this.isPublic;
    }

    public int getRound() {
        return this.round;
    }

    public int getNextRound() {
        return this.round + 1;
    }

    public void setStarted(boolean started) {
        this.started = started;
        this.markDirty(TOURNAMENTS.STARTED, this.started);
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
        this.markDirty(TOURNAMENTS.ENDED, this.ended);
    }

    public void toggleSignups() {
        this.setAllowSignups(!this.allowSignups());
    }

    public void setAllowSignups(boolean allowSignups) {
        this.allowSignups = allowSignups;
        this.markDirty(TOURNAMENTS.ALLOW_SIGNUPS, this.allowSignups);
    }

    public void setPublic(boolean isPublic) {
        this.isPublic = isPublic;
        this.markDirty(TOURNAMENTS.PUBLIC, this.isPublic);
    }

    public void nextRound() {
        this.round = this.round + 1;
        this.markDirty(TOURNAMENTS.ROUND, this.round);
    }

    public boolean hasResultBeenSubmitted(int round) {
        return this.dbh
                .fetchExists(
                        select()
                        .from(GAMES_LIVE)
                        .where(GAMES_LIVE.TOURNAMENT_ID.eq(this.id).and(GAMES_LIVE.ROUND.eq(round)).and(GAMES_LIVE.WINNER.ne(""))));
    }

    public void reset() {
        this.setStarted(false);
        this.round = 0;
        this.markDirty(TOURNAMENTS.ROUND, 0);
        this.pushUpdate();

        this.dbh.execute(
           update(COMPETITORS)
          .set(COMPETITORS.WINS, 0)
          .set(COMPETITORS.MVPS, 0)
          .where(COMPETITORS.TOURNAMENT_ID.eq(this.id)));

        this.dbh.execute(
          delete(GAMES_LIVE)
          .where(GAMES_LIVE.TOURNAMENT_ID.eq(this.id)));
    }

    public List<Game> getAllGames() {
        List<Integer> games = this.dbh.getCtx(
          select(GAMES_LIVE.GAME_ID)
          .from(GAMES_LIVE)
          .where(GAMES_LIVE.TOURNAMENT_ID.eq(this.id)))
          .fetch(GAMES_LIVE.GAME_ID);

        return Game.get(games);
    }

    public List<Game> getGamesForRound(int round) {
        if (round < 1 || round > this.round) {
            return Collections.emptyList();
        }

        List<Integer> games = this.dbh.getCtx(
           select(GAMES_LIVE.GAME_ID)
          .from(GAMES_LIVE)
          .where(GAMES_LIVE.TOURNAMENT_ID.eq(this.id).and(GAMES_LIVE.ROUND.eq(round))))
          .fetch(GAMES_LIVE.GAME_ID);

        return Game.get(games);
    }

    private static final Map<String, Tournament> CACHE = new HashMap<>();

    private static boolean exists(DatabaseConnection dbh, String tournamentId) {
        return dbh.fetchExists(select()
                .from(TOURNAMENTS)
                .where(TOURNAMENTS.ID.eq(tournamentId)));
    }

    /**
     * Gets the tournament from the cache
     */
    public static Tournament get(DatabaseConnection dbh, String tournamentId, boolean create) {
        if (tournamentId == null || (!create && !exists(dbh, tournamentId))) {
            return null;
        }

        return CACHE.getOrDefault(tournamentId, new Tournament(dbh, tournamentId));
    }

    public static Tournament get(DatabaseConnection dbh, String tournamentId) {
        return get(dbh, tournamentId, false);
    }

    public enum Type {
        SUMMONERS_RIFT(),
        ARAM();

        public static Type byName(String name) {
            for (Type type : Type.values()) {
                if (type.name().equalsIgnoreCase(name)) {
                    return type;
                }
            }

            throw new NoSuchElementException("Unknown tier name: " + name);
        }

        public static Type byIndex(int index) {
            Type[] types = Type.values();
            if (index < 0 || index >= types.length) {
                throw new NoSuchElementException("Unknown tier name: " + index);
            }

            return types[index];
        }

        public static Type byIndex(Number num) {
            return byIndex(num.intValue());
        }
    }

    @Override
    public Condition getCondition() {
        return TOURNAMENTS.ID.eq(this.id);
    }
}
