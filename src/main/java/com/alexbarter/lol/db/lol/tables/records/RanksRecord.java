/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.lol.tables.records;


import com.alexbarter.lol.db.lol.tables.Ranks;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.UInteger;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class RanksRecord extends UpdatableRecordImpl<RanksRecord> implements Record4<String, String, Integer, UInteger> {

    private static final long serialVersionUID = 497410614;

    /**
     * Setter for <code>lol.ranks.tier</code>.
     */
    public void setTier(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>lol.ranks.tier</code>.
     */
    public String getTier() {
        return (String) get(0);
    }

    /**
     * Setter for <code>lol.ranks.summoner_id</code>.
     */
    public void setSummonerId(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>lol.ranks.summoner_id</code>.
     */
    public String getSummonerId() {
        return (String) get(1);
    }

    /**
     * Setter for <code>lol.ranks.lp</code>.
     */
    public void setLp(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>lol.ranks.lp</code>.
     */
    public Integer getLp() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>lol.ranks.division</code>.
     */
    public void setDivision(UInteger value) {
        set(3, value);
    }

    /**
     * Getter for <code>lol.ranks.division</code>.
     */
    public UInteger getDivision() {
        return (UInteger) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<String, String, Integer, UInteger> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<String, String, Integer, UInteger> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Ranks.RANKS.TIER;
    }

    @Override
    public Field<String> field2() {
        return Ranks.RANKS.SUMMONER_ID;
    }

    @Override
    public Field<Integer> field3() {
        return Ranks.RANKS.LP;
    }

    @Override
    public Field<UInteger> field4() {
        return Ranks.RANKS.DIVISION;
    }

    @Override
    public String component1() {
        return getTier();
    }

    @Override
    public String component2() {
        return getSummonerId();
    }

    @Override
    public Integer component3() {
        return getLp();
    }

    @Override
    public UInteger component4() {
        return getDivision();
    }

    @Override
    public String value1() {
        return getTier();
    }

    @Override
    public String value2() {
        return getSummonerId();
    }

    @Override
    public Integer value3() {
        return getLp();
    }

    @Override
    public UInteger value4() {
        return getDivision();
    }

    @Override
    public RanksRecord value1(String value) {
        setTier(value);
        return this;
    }

    @Override
    public RanksRecord value2(String value) {
        setSummonerId(value);
        return this;
    }

    @Override
    public RanksRecord value3(Integer value) {
        setLp(value);
        return this;
    }

    @Override
    public RanksRecord value4(UInteger value) {
        setDivision(value);
        return this;
    }

    @Override
    public RanksRecord values(String value1, String value2, Integer value3, UInteger value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached RanksRecord
     */
    public RanksRecord() {
        super(Ranks.RANKS);
    }

    /**
     * Create a detached, initialised RanksRecord
     */
    public RanksRecord(String tier, String summonerId, Integer lp, UInteger division) {
        super(Ranks.RANKS);

        set(0, tier);
        set(1, summonerId);
        set(2, lp);
        set(3, division);
    }
}
