/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.lol.tables;


import com.alexbarter.lol.db.lol.Indexes;
import com.alexbarter.lol.db.lol.Keys;
import com.alexbarter.lol.db.lol.Lol;
import com.alexbarter.lol.db.lol.tables.records.CompetitorsRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row5;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.UByte;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Competitors extends TableImpl<CompetitorsRecord> {

    private static final long serialVersionUID = -2136738053;

    /**
     * The reference instance of <code>lol.competitors</code>
     */
    public static final Competitors COMPETITORS = new Competitors();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<CompetitorsRecord> getRecordType() {
        return CompetitorsRecord.class;
    }

    /**
     * The column <code>lol.competitors.tournament_id</code>.
     */
    public final TableField<CompetitorsRecord, String> TOURNAMENT_ID = createField(DSL.name("tournament_id"), org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false), this, "");

    /**
     * The column <code>lol.competitors.summoner_id</code>.
     */
    public final TableField<CompetitorsRecord, String> SUMMONER_ID = createField(DSL.name("summoner_id"), org.jooq.impl.SQLDataType.VARCHAR(63).nullable(false), this, "");

    /**
     * The column <code>lol.competitors.wins</code>.
     */
    public final TableField<CompetitorsRecord, Integer> WINS = createField(DSL.name("wins"), org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("0", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>lol.competitors.mvps</code>.
     */
    public final TableField<CompetitorsRecord, Integer> MVPS = createField(DSL.name("mvps"), org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("0", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * The column <code>lol.competitors.playing</code>.
     */
    public final TableField<CompetitorsRecord, UByte> PLAYING = createField(DSL.name("playing"), org.jooq.impl.SQLDataType.TINYINTUNSIGNED.nullable(false).defaultValue(org.jooq.impl.DSL.field("1", org.jooq.impl.SQLDataType.TINYINTUNSIGNED)), this, "");

    /**
     * Create a <code>lol.competitors</code> table reference
     */
    public Competitors() {
        this(DSL.name("competitors"), null);
    }

    /**
     * Create an aliased <code>lol.competitors</code> table reference
     */
    public Competitors(String alias) {
        this(DSL.name(alias), COMPETITORS);
    }

    /**
     * Create an aliased <code>lol.competitors</code> table reference
     */
    public Competitors(Name alias) {
        this(alias, COMPETITORS);
    }

    private Competitors(Name alias, Table<CompetitorsRecord> aliased) {
        this(alias, aliased, null);
    }

    private Competitors(Name alias, Table<CompetitorsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Competitors(Table<O> child, ForeignKey<O, CompetitorsRecord> key) {
        super(child, key, COMPETITORS);
    }

    @Override
    public Schema getSchema() {
        return Lol.LOL;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.COMPETITORS_PRIMARY);
    }

    @Override
    public UniqueKey<CompetitorsRecord> getPrimaryKey() {
        return Keys.KEY_COMPETITORS_PRIMARY;
    }

    @Override
    public List<UniqueKey<CompetitorsRecord>> getKeys() {
        return Arrays.<UniqueKey<CompetitorsRecord>>asList(Keys.KEY_COMPETITORS_PRIMARY);
    }

    @Override
    public Competitors as(String alias) {
        return new Competitors(DSL.name(alias), this);
    }

    @Override
    public Competitors as(Name alias) {
        return new Competitors(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Competitors rename(String name) {
        return new Competitors(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Competitors rename(Name name) {
        return new Competitors(name, null);
    }

    // -------------------------------------------------------------------------
    // Row5 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row5<String, String, Integer, Integer, UByte> fieldsRow() {
        return (Row5) super.fieldsRow();
    }
}
