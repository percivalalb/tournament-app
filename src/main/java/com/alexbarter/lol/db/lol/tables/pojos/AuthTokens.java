/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.lol.tables.pojos;


import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AuthTokens implements Serializable {

    private static final long serialVersionUID = -486403149;

    private String    token;
    private Timestamp created;
    private Timestamp expires;
    private Integer   maxUses;

    public AuthTokens() {}

    public AuthTokens(AuthTokens value) {
        this.token = value.token;
        this.created = value.created;
        this.expires = value.expires;
        this.maxUses = value.maxUses;
    }

    public AuthTokens(
        String    token,
        Timestamp created,
        Timestamp expires,
        Integer   maxUses
    ) {
        this.token = token;
        this.created = created;
        this.expires = expires;
        this.maxUses = maxUses;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getCreated() {
        return this.created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getExpires() {
        return this.expires;
    }

    public void setExpires(Timestamp expires) {
        this.expires = expires;
    }

    public Integer getMaxUses() {
        return this.maxUses;
    }

    public void setMaxUses(Integer maxUses) {
        this.maxUses = maxUses;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("AuthTokens (");

        sb.append(token);
        sb.append(", ").append(created);
        sb.append(", ").append(expires);
        sb.append(", ").append(maxUses);

        sb.append(")");
        return sb.toString();
    }
}
