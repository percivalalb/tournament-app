/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.lol.tables;


import com.alexbarter.lol.db.lol.Indexes;
import com.alexbarter.lol.db.lol.Keys;
import com.alexbarter.lol.db.lol.Lol;
import com.alexbarter.lol.db.lol.enums.RolesAvoidRole;
import com.alexbarter.lol.db.lol.enums.RolesPrimaryRole;
import com.alexbarter.lol.db.lol.enums.RolesSecondaryRole;
import com.alexbarter.lol.db.lol.tables.records.RolesRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row5;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.UByte;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Roles extends TableImpl<RolesRecord> {

    private static final long serialVersionUID = 1620575787;

    /**
     * The reference instance of <code>lol.roles</code>
     */
    public static final Roles ROLES = new Roles();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<RolesRecord> getRecordType() {
        return RolesRecord.class;
    }

    /**
     * The column <code>lol.roles.summoner_id</code>.
     */
    public final TableField<RolesRecord, String> SUMMONER_ID = createField(DSL.name("summoner_id"), org.jooq.impl.SQLDataType.VARCHAR(63).nullable(false), this, "");

    /**
     * The column <code>lol.roles.primary_role</code>.
     */
    public final TableField<RolesRecord, RolesPrimaryRole> PRIMARY_ROLE = createField(DSL.name("primary_role"), org.jooq.impl.SQLDataType.VARCHAR(7).nullable(false).asEnumDataType(com.alexbarter.lol.db.lol.enums.RolesPrimaryRole.class), this, "");

    /**
     * The column <code>lol.roles.secondary_role</code>.
     */
    public final TableField<RolesRecord, RolesSecondaryRole> SECONDARY_ROLE = createField(DSL.name("secondary_role"), org.jooq.impl.SQLDataType.VARCHAR(7).nullable(false).asEnumDataType(com.alexbarter.lol.db.lol.enums.RolesSecondaryRole.class), this, "");

    /**
     * The column <code>lol.roles.fill</code>.
     */
    public final TableField<RolesRecord, UByte> FILL = createField(DSL.name("fill"), org.jooq.impl.SQLDataType.TINYINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>lol.roles.avoid_role</code>.
     */
    public final TableField<RolesRecord, RolesAvoidRole> AVOID_ROLE = createField(DSL.name("avoid_role"), org.jooq.impl.SQLDataType.VARCHAR(7).defaultValue(org.jooq.impl.DSL.field("NULL", org.jooq.impl.SQLDataType.VARCHAR)).asEnumDataType(com.alexbarter.lol.db.lol.enums.RolesAvoidRole.class), this, "");

    /**
     * Create a <code>lol.roles</code> table reference
     */
    public Roles() {
        this(DSL.name("roles"), null);
    }

    /**
     * Create an aliased <code>lol.roles</code> table reference
     */
    public Roles(String alias) {
        this(DSL.name(alias), ROLES);
    }

    /**
     * Create an aliased <code>lol.roles</code> table reference
     */
    public Roles(Name alias) {
        this(alias, ROLES);
    }

    private Roles(Name alias, Table<RolesRecord> aliased) {
        this(alias, aliased, null);
    }

    private Roles(Name alias, Table<RolesRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Roles(Table<O> child, ForeignKey<O, RolesRecord> key) {
        super(child, key, ROLES);
    }

    @Override
    public Schema getSchema() {
        return Lol.LOL;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.ROLES_PRIMARY);
    }

    @Override
    public UniqueKey<RolesRecord> getPrimaryKey() {
        return Keys.KEY_ROLES_PRIMARY;
    }

    @Override
    public List<UniqueKey<RolesRecord>> getKeys() {
        return Arrays.<UniqueKey<RolesRecord>>asList(Keys.KEY_ROLES_PRIMARY);
    }

    @Override
    public Roles as(String alias) {
        return new Roles(DSL.name(alias), this);
    }

    @Override
    public Roles as(Name alias) {
        return new Roles(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Roles rename(String name) {
        return new Roles(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Roles rename(Name name) {
        return new Roles(name, null);
    }

    // -------------------------------------------------------------------------
    // Row5 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row5<String, RolesPrimaryRole, RolesSecondaryRole, UByte, RolesAvoidRole> fieldsRow() {
        return (Row5) super.fieldsRow();
    }
}
