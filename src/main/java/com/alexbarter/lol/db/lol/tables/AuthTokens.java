/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.lol.tables;


import com.alexbarter.lol.db.lol.Lol;
import com.alexbarter.lol.db.lol.tables.records.AuthTokensRecord;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row4;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AuthTokens extends TableImpl<AuthTokensRecord> {

    private static final long serialVersionUID = 93939585;

    /**
     * The reference instance of <code>lol.auth_tokens</code>
     */
    public static final AuthTokens AUTH_TOKENS = new AuthTokens();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<AuthTokensRecord> getRecordType() {
        return AuthTokensRecord.class;
    }

    /**
     * The column <code>lol.auth_tokens.token</code>.
     */
    public final TableField<AuthTokensRecord, String> TOKEN = createField(DSL.name("token"), org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false), this, "");

    /**
     * The column <code>lol.auth_tokens.created</code>.
     */
    public final TableField<AuthTokensRecord, Timestamp> CREATED = createField(DSL.name("created"), org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.field("current_timestamp()", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>lol.auth_tokens.expires</code>.
     */
    public final TableField<AuthTokensRecord, Timestamp> EXPIRES = createField(DSL.name("expires"), org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "");

    /**
     * The column <code>lol.auth_tokens.max_uses</code>.
     */
    public final TableField<AuthTokensRecord, Integer> MAX_USES = createField(DSL.name("max_uses"), org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("-1", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * Create a <code>lol.auth_tokens</code> table reference
     */
    public AuthTokens() {
        this(DSL.name("auth_tokens"), null);
    }

    /**
     * Create an aliased <code>lol.auth_tokens</code> table reference
     */
    public AuthTokens(String alias) {
        this(DSL.name(alias), AUTH_TOKENS);
    }

    /**
     * Create an aliased <code>lol.auth_tokens</code> table reference
     */
    public AuthTokens(Name alias) {
        this(alias, AUTH_TOKENS);
    }

    private AuthTokens(Name alias, Table<AuthTokensRecord> aliased) {
        this(alias, aliased, null);
    }

    private AuthTokens(Name alias, Table<AuthTokensRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> AuthTokens(Table<O> child, ForeignKey<O, AuthTokensRecord> key) {
        super(child, key, AUTH_TOKENS);
    }

    @Override
    public Schema getSchema() {
        return Lol.LOL;
    }

    @Override
    public AuthTokens as(String alias) {
        return new AuthTokens(DSL.name(alias), this);
    }

    @Override
    public AuthTokens as(Name alias) {
        return new AuthTokens(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public AuthTokens rename(String name) {
        return new AuthTokens(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public AuthTokens rename(Name name) {
        return new AuthTokens(name, null);
    }

    // -------------------------------------------------------------------------
    // Row4 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row4<String, Timestamp, Timestamp, Integer> fieldsRow() {
        return (Row4) super.fieldsRow();
    }
}
