/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.lol.tables.records;


import com.alexbarter.lol.db.lol.tables.AuthTokens;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AuthTokensRecord extends TableRecordImpl<AuthTokensRecord> implements Record4<String, Timestamp, Timestamp, Integer> {

    private static final long serialVersionUID = -2074520870;

    /**
     * Setter for <code>lol.auth_tokens.token</code>.
     */
    public void setToken(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>lol.auth_tokens.token</code>.
     */
    public String getToken() {
        return (String) get(0);
    }

    /**
     * Setter for <code>lol.auth_tokens.created</code>.
     */
    public void setCreated(Timestamp value) {
        set(1, value);
    }

    /**
     * Getter for <code>lol.auth_tokens.created</code>.
     */
    public Timestamp getCreated() {
        return (Timestamp) get(1);
    }

    /**
     * Setter for <code>lol.auth_tokens.expires</code>.
     */
    public void setExpires(Timestamp value) {
        set(2, value);
    }

    /**
     * Getter for <code>lol.auth_tokens.expires</code>.
     */
    public Timestamp getExpires() {
        return (Timestamp) get(2);
    }

    /**
     * Setter for <code>lol.auth_tokens.max_uses</code>.
     */
    public void setMaxUses(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>lol.auth_tokens.max_uses</code>.
     */
    public Integer getMaxUses() {
        return (Integer) get(3);
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<String, Timestamp, Timestamp, Integer> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<String, Timestamp, Timestamp, Integer> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return AuthTokens.AUTH_TOKENS.TOKEN;
    }

    @Override
    public Field<Timestamp> field2() {
        return AuthTokens.AUTH_TOKENS.CREATED;
    }

    @Override
    public Field<Timestamp> field3() {
        return AuthTokens.AUTH_TOKENS.EXPIRES;
    }

    @Override
    public Field<Integer> field4() {
        return AuthTokens.AUTH_TOKENS.MAX_USES;
    }

    @Override
    public String component1() {
        return getToken();
    }

    @Override
    public Timestamp component2() {
        return getCreated();
    }

    @Override
    public Timestamp component3() {
        return getExpires();
    }

    @Override
    public Integer component4() {
        return getMaxUses();
    }

    @Override
    public String value1() {
        return getToken();
    }

    @Override
    public Timestamp value2() {
        return getCreated();
    }

    @Override
    public Timestamp value3() {
        return getExpires();
    }

    @Override
    public Integer value4() {
        return getMaxUses();
    }

    @Override
    public AuthTokensRecord value1(String value) {
        setToken(value);
        return this;
    }

    @Override
    public AuthTokensRecord value2(Timestamp value) {
        setCreated(value);
        return this;
    }

    @Override
    public AuthTokensRecord value3(Timestamp value) {
        setExpires(value);
        return this;
    }

    @Override
    public AuthTokensRecord value4(Integer value) {
        setMaxUses(value);
        return this;
    }

    @Override
    public AuthTokensRecord values(String value1, Timestamp value2, Timestamp value3, Integer value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached AuthTokensRecord
     */
    public AuthTokensRecord() {
        super(AuthTokens.AUTH_TOKENS);
    }

    /**
     * Create a detached, initialised AuthTokensRecord
     */
    public AuthTokensRecord(String token, Timestamp created, Timestamp expires, Integer maxUses) {
        super(AuthTokens.AUTH_TOKENS);

        set(0, token);
        set(1, created);
        set(2, expires);
        set(3, maxUses);
    }
}
