/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.lol.tables.records;


import com.alexbarter.lol.db.lol.tables.Accounts;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AccountsRecord extends UpdatableRecordImpl<AccountsRecord> implements Record5<String, String, String, Integer, String> {

    private static final long serialVersionUID = 2049132614;

    /**
     * Setter for <code>lol.accounts.account_id</code>.
     */
    public void setAccountId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>lol.accounts.account_id</code>.
     */
    public String getAccountId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>lol.accounts.summoner_id</code>.
     */
    public void setSummonerId(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>lol.accounts.summoner_id</code>.
     */
    public String getSummonerId() {
        return (String) get(1);
    }

    /**
     * Setter for <code>lol.accounts.puuid</code>.
     */
    public void setPuuid(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>lol.accounts.puuid</code>.
     */
    public String getPuuid() {
        return (String) get(2);
    }

    /**
     * Setter for <code>lol.accounts.player_id</code>.
     */
    public void setPlayerId(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>lol.accounts.player_id</code>.
     */
    public Integer getPlayerId() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>lol.accounts.name</code>.
     */
    public void setName(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>lol.accounts.name</code>.
     */
    public String getName() {
        return (String) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row5<String, String, String, Integer, String> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    @Override
    public Row5<String, String, String, Integer, String> valuesRow() {
        return (Row5) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Accounts.ACCOUNTS.ACCOUNT_ID;
    }

    @Override
    public Field<String> field2() {
        return Accounts.ACCOUNTS.SUMMONER_ID;
    }

    @Override
    public Field<String> field3() {
        return Accounts.ACCOUNTS.PUUID;
    }

    @Override
    public Field<Integer> field4() {
        return Accounts.ACCOUNTS.PLAYER_ID;
    }

    @Override
    public Field<String> field5() {
        return Accounts.ACCOUNTS.NAME;
    }

    @Override
    public String component1() {
        return getAccountId();
    }

    @Override
    public String component2() {
        return getSummonerId();
    }

    @Override
    public String component3() {
        return getPuuid();
    }

    @Override
    public Integer component4() {
        return getPlayerId();
    }

    @Override
    public String component5() {
        return getName();
    }

    @Override
    public String value1() {
        return getAccountId();
    }

    @Override
    public String value2() {
        return getSummonerId();
    }

    @Override
    public String value3() {
        return getPuuid();
    }

    @Override
    public Integer value4() {
        return getPlayerId();
    }

    @Override
    public String value5() {
        return getName();
    }

    @Override
    public AccountsRecord value1(String value) {
        setAccountId(value);
        return this;
    }

    @Override
    public AccountsRecord value2(String value) {
        setSummonerId(value);
        return this;
    }

    @Override
    public AccountsRecord value3(String value) {
        setPuuid(value);
        return this;
    }

    @Override
    public AccountsRecord value4(Integer value) {
        setPlayerId(value);
        return this;
    }

    @Override
    public AccountsRecord value5(String value) {
        setName(value);
        return this;
    }

    @Override
    public AccountsRecord values(String value1, String value2, String value3, Integer value4, String value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached AccountsRecord
     */
    public AccountsRecord() {
        super(Accounts.ACCOUNTS);
    }

    /**
     * Create a detached, initialised AccountsRecord
     */
    public AccountsRecord(String accountId, String summonerId, String puuid, Integer playerId, String name) {
        super(Accounts.ACCOUNTS);

        set(0, accountId);
        set(1, summonerId);
        set(2, puuid);
        set(3, playerId);
        set(4, name);
    }
}
