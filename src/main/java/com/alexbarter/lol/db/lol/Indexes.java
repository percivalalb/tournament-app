/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.lol;


import com.alexbarter.lol.db.lol.tables.Accounts;
import com.alexbarter.lol.db.lol.tables.Competitors;
import com.alexbarter.lol.db.lol.tables.Discords;
import com.alexbarter.lol.db.lol.tables.Games;
import com.alexbarter.lol.db.lol.tables.GamesLive;
import com.alexbarter.lol.db.lol.tables.Players;
import com.alexbarter.lol.db.lol.tables.Ranks;
import com.alexbarter.lol.db.lol.tables.Roles;
import com.alexbarter.lol.db.lol.tables.Tournaments;

import javax.annotation.Generated;

import org.jooq.Index;
import org.jooq.OrderField;
import org.jooq.impl.Internal;


/**
 * A class modelling indexes of tables of the <code>lol</code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Indexes {

    // -------------------------------------------------------------------------
    // INDEX definitions
    // -------------------------------------------------------------------------

    public static final Index ACCOUNTS_INDEX_ACCOUNT_ID = Indexes0.ACCOUNTS_INDEX_ACCOUNT_ID;
    public static final Index ACCOUNTS_INDEX_PLAYER_ID = Indexes0.ACCOUNTS_INDEX_PLAYER_ID;
    public static final Index ACCOUNTS_PRIMARY = Indexes0.ACCOUNTS_PRIMARY;
    public static final Index COMPETITORS_PRIMARY = Indexes0.COMPETITORS_PRIMARY;
    public static final Index DISCORDS_PRIMARY = Indexes0.DISCORDS_PRIMARY;
    public static final Index GAMES_PRIMARY = Indexes0.GAMES_PRIMARY;
    public static final Index GAMES_LIVE_PRIMARY = Indexes0.GAMES_LIVE_PRIMARY;
    public static final Index PLAYERS_INDEX_PLAYER_ID = Indexes0.PLAYERS_INDEX_PLAYER_ID;
    public static final Index PLAYERS_PRIMARY = Indexes0.PLAYERS_PRIMARY;
    public static final Index RANKS_PRIMARY = Indexes0.RANKS_PRIMARY;
    public static final Index ROLES_PRIMARY = Indexes0.ROLES_PRIMARY;
    public static final Index TOURNAMENTS_PRIMARY = Indexes0.TOURNAMENTS_PRIMARY;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Indexes0 {
        public static Index ACCOUNTS_INDEX_ACCOUNT_ID = Internal.createIndex("index_account_id", Accounts.ACCOUNTS, new OrderField[] { Accounts.ACCOUNTS.ACCOUNT_ID }, true);
        public static Index ACCOUNTS_INDEX_PLAYER_ID = Internal.createIndex("index_player_id", Accounts.ACCOUNTS, new OrderField[] { Accounts.ACCOUNTS.PLAYER_ID }, false);
        public static Index ACCOUNTS_PRIMARY = Internal.createIndex("PRIMARY", Accounts.ACCOUNTS, new OrderField[] { Accounts.ACCOUNTS.ACCOUNT_ID }, true);
        public static Index COMPETITORS_PRIMARY = Internal.createIndex("PRIMARY", Competitors.COMPETITORS, new OrderField[] { Competitors.COMPETITORS.TOURNAMENT_ID, Competitors.COMPETITORS.SUMMONER_ID }, true);
        public static Index DISCORDS_PRIMARY = Internal.createIndex("PRIMARY", Discords.DISCORDS, new OrderField[] { Discords.DISCORDS.SUMMONER_ID }, true);
        public static Index GAMES_PRIMARY = Internal.createIndex("PRIMARY", Games.GAMES, new OrderField[] { Games.GAMES.GAME_ID }, true);
        public static Index GAMES_LIVE_PRIMARY = Internal.createIndex("PRIMARY", GamesLive.GAMES_LIVE, new OrderField[] { GamesLive.GAMES_LIVE.GAME_ID }, true);
        public static Index PLAYERS_INDEX_PLAYER_ID = Internal.createIndex("index_player_id", Players.PLAYERS, new OrderField[] { Players.PLAYERS.PLAYER_ID }, true);
        public static Index PLAYERS_PRIMARY = Internal.createIndex("PRIMARY", Players.PLAYERS, new OrderField[] { Players.PLAYERS.PLAYER_ID }, true);
        public static Index RANKS_PRIMARY = Internal.createIndex("PRIMARY", Ranks.RANKS, new OrderField[] { Ranks.RANKS.SUMMONER_ID }, true);
        public static Index ROLES_PRIMARY = Internal.createIndex("PRIMARY", Roles.ROLES, new OrderField[] { Roles.ROLES.SUMMONER_ID }, true);
        public static Index TOURNAMENTS_PRIMARY = Internal.createIndex("PRIMARY", Tournaments.TOURNAMENTS, new OrderField[] { Tournaments.TOURNAMENTS.ID }, true);
    }
}
