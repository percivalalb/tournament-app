/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.tables.daos;


import com.alexbarter.lol.db.mysql.tables.ColumnsPriv;
import com.alexbarter.lol.db.mysql.tables.records.ColumnsPrivRecord;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.Record5;
import org.jooq.impl.DAOImpl;


/**
 * Column privileges
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ColumnsPrivDao extends DAOImpl<ColumnsPrivRecord, com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv, Record5<String, String, String, String, String>> {

    /**
     * Create a new ColumnsPrivDao without any configuration
     */
    public ColumnsPrivDao() {
        super(ColumnsPriv.COLUMNS_PRIV, com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv.class);
    }

    /**
     * Create a new ColumnsPrivDao with an attached configuration
     */
    public ColumnsPrivDao(Configuration configuration) {
        super(ColumnsPriv.COLUMNS_PRIV, com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv.class, configuration);
    }

    @Override
    public Record5<String, String, String, String, String> getId(com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv object) {
        return compositeKeyRecord(object.getHost(), object.getDb(), object.getUser(), object.getTableName(), object.getColumnName());
    }

    /**
     * Fetch records that have <code>Host BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchRangeOfHost(String lowerInclusive, String upperInclusive) {
        return fetchRange(ColumnsPriv.COLUMNS_PRIV.HOST, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Host IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchByHost(String... values) {
        return fetch(ColumnsPriv.COLUMNS_PRIV.HOST, values);
    }

    /**
     * Fetch records that have <code>Db BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchRangeOfDb(String lowerInclusive, String upperInclusive) {
        return fetchRange(ColumnsPriv.COLUMNS_PRIV.DB, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Db IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchByDb(String... values) {
        return fetch(ColumnsPriv.COLUMNS_PRIV.DB, values);
    }

    /**
     * Fetch records that have <code>User BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchRangeOfUser(String lowerInclusive, String upperInclusive) {
        return fetchRange(ColumnsPriv.COLUMNS_PRIV.USER, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>User IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchByUser(String... values) {
        return fetch(ColumnsPriv.COLUMNS_PRIV.USER, values);
    }

    /**
     * Fetch records that have <code>Table_name BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchRangeOfTableName(String lowerInclusive, String upperInclusive) {
        return fetchRange(ColumnsPriv.COLUMNS_PRIV.TABLE_NAME, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Table_name IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchByTableName(String... values) {
        return fetch(ColumnsPriv.COLUMNS_PRIV.TABLE_NAME, values);
    }

    /**
     * Fetch records that have <code>Column_name BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchRangeOfColumnName(String lowerInclusive, String upperInclusive) {
        return fetchRange(ColumnsPriv.COLUMNS_PRIV.COLUMN_NAME, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Column_name IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchByColumnName(String... values) {
        return fetch(ColumnsPriv.COLUMNS_PRIV.COLUMN_NAME, values);
    }

    /**
     * Fetch records that have <code>Timestamp BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchRangeOfTimestamp(Timestamp lowerInclusive, Timestamp upperInclusive) {
        return fetchRange(ColumnsPriv.COLUMNS_PRIV.TIMESTAMP, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Timestamp IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchByTimestamp(Timestamp... values) {
        return fetch(ColumnsPriv.COLUMNS_PRIV.TIMESTAMP, values);
    }

    /**
     * Fetch records that have <code>Column_priv BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchRangeOfColumnPriv(String lowerInclusive, String upperInclusive) {
        return fetchRange(ColumnsPriv.COLUMNS_PRIV.COLUMN_PRIV, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Column_priv IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.ColumnsPriv> fetchByColumnPriv(String... values) {
        return fetch(ColumnsPriv.COLUMNS_PRIV.COLUMN_PRIV, values);
    }
}
