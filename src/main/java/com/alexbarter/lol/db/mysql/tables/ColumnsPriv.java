/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.tables;


import com.alexbarter.lol.db.mysql.Indexes;
import com.alexbarter.lol.db.mysql.Keys;
import com.alexbarter.lol.db.mysql.Mysql;
import com.alexbarter.lol.db.mysql.tables.records.ColumnsPrivRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row7;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * Column privileges
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ColumnsPriv extends TableImpl<ColumnsPrivRecord> {

    private static final long serialVersionUID = 1626997832;

    /**
     * The reference instance of <code>mysql.columns_priv</code>
     */
    public static final ColumnsPriv COLUMNS_PRIV = new ColumnsPriv();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ColumnsPrivRecord> getRecordType() {
        return ColumnsPrivRecord.class;
    }

    /**
     * The column <code>mysql.columns_priv.Host</code>.
     */
    public final TableField<ColumnsPrivRecord, String> HOST = createField(DSL.name("Host"), org.jooq.impl.SQLDataType.CHAR(60).nullable(false).defaultValue(org.jooq.impl.DSL.field("''", org.jooq.impl.SQLDataType.CHAR)), this, "");

    /**
     * The column <code>mysql.columns_priv.Db</code>.
     */
    public final TableField<ColumnsPrivRecord, String> DB = createField(DSL.name("Db"), org.jooq.impl.SQLDataType.CHAR(64).nullable(false).defaultValue(org.jooq.impl.DSL.field("''", org.jooq.impl.SQLDataType.CHAR)), this, "");

    /**
     * The column <code>mysql.columns_priv.User</code>.
     */
    public final TableField<ColumnsPrivRecord, String> USER = createField(DSL.name("User"), org.jooq.impl.SQLDataType.CHAR(80).nullable(false).defaultValue(org.jooq.impl.DSL.field("''", org.jooq.impl.SQLDataType.CHAR)), this, "");

    /**
     * The column <code>mysql.columns_priv.Table_name</code>.
     */
    public final TableField<ColumnsPrivRecord, String> TABLE_NAME = createField(DSL.name("Table_name"), org.jooq.impl.SQLDataType.CHAR(64).nullable(false).defaultValue(org.jooq.impl.DSL.field("''", org.jooq.impl.SQLDataType.CHAR)), this, "");

    /**
     * The column <code>mysql.columns_priv.Column_name</code>.
     */
    public final TableField<ColumnsPrivRecord, String> COLUMN_NAME = createField(DSL.name("Column_name"), org.jooq.impl.SQLDataType.CHAR(64).nullable(false).defaultValue(org.jooq.impl.DSL.field("''", org.jooq.impl.SQLDataType.CHAR)), this, "");

    /**
     * The column <code>mysql.columns_priv.Timestamp</code>.
     */
    public final TableField<ColumnsPrivRecord, Timestamp> TIMESTAMP = createField(DSL.name("Timestamp"), org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.field("current_timestamp()", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>mysql.columns_priv.Column_priv</code>.
     */
    public final TableField<ColumnsPrivRecord, String> COLUMN_PRIV = createField(DSL.name("Column_priv"), org.jooq.impl.SQLDataType.VARCHAR(31).nullable(false).defaultValue(org.jooq.impl.DSL.field("''", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * Create a <code>mysql.columns_priv</code> table reference
     */
    public ColumnsPriv() {
        this(DSL.name("columns_priv"), null);
    }

    /**
     * Create an aliased <code>mysql.columns_priv</code> table reference
     */
    public ColumnsPriv(String alias) {
        this(DSL.name(alias), COLUMNS_PRIV);
    }

    /**
     * Create an aliased <code>mysql.columns_priv</code> table reference
     */
    public ColumnsPriv(Name alias) {
        this(alias, COLUMNS_PRIV);
    }

    private ColumnsPriv(Name alias, Table<ColumnsPrivRecord> aliased) {
        this(alias, aliased, null);
    }

    private ColumnsPriv(Name alias, Table<ColumnsPrivRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment("Column privileges"));
    }

    public <O extends Record> ColumnsPriv(Table<O> child, ForeignKey<O, ColumnsPrivRecord> key) {
        super(child, key, COLUMNS_PRIV);
    }

    @Override
    public Schema getSchema() {
        return Mysql.MYSQL;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.COLUMNS_PRIV_PRIMARY);
    }

    @Override
    public UniqueKey<ColumnsPrivRecord> getPrimaryKey() {
        return Keys.KEY_COLUMNS_PRIV_PRIMARY;
    }

    @Override
    public List<UniqueKey<ColumnsPrivRecord>> getKeys() {
        return Arrays.<UniqueKey<ColumnsPrivRecord>>asList(Keys.KEY_COLUMNS_PRIV_PRIMARY);
    }

    @Override
    public ColumnsPriv as(String alias) {
        return new ColumnsPriv(DSL.name(alias), this);
    }

    @Override
    public ColumnsPriv as(Name alias) {
        return new ColumnsPriv(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public ColumnsPriv rename(String name) {
        return new ColumnsPriv(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public ColumnsPriv rename(Name name) {
        return new ColumnsPriv(name, null);
    }

    // -------------------------------------------------------------------------
    // Row7 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row7<String, String, String, String, String, Timestamp, String> fieldsRow() {
        return (Row7) super.fieldsRow();
    }
}
