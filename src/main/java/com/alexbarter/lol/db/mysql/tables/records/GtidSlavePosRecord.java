/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.tables.records;


import com.alexbarter.lol.db.mysql.tables.GtidSlavePos;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.UInteger;
import org.jooq.types.ULong;


/**
 * Replication slave GTID position
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class GtidSlavePosRecord extends UpdatableRecordImpl<GtidSlavePosRecord> implements Record4<UInteger, ULong, UInteger, ULong> {

    private static final long serialVersionUID = -356739987;

    /**
     * Setter for <code>mysql.gtid_slave_pos.domain_id</code>.
     */
    public void setDomainId(UInteger value) {
        set(0, value);
    }

    /**
     * Getter for <code>mysql.gtid_slave_pos.domain_id</code>.
     */
    public UInteger getDomainId() {
        return (UInteger) get(0);
    }

    /**
     * Setter for <code>mysql.gtid_slave_pos.sub_id</code>.
     */
    public void setSubId(ULong value) {
        set(1, value);
    }

    /**
     * Getter for <code>mysql.gtid_slave_pos.sub_id</code>.
     */
    public ULong getSubId() {
        return (ULong) get(1);
    }

    /**
     * Setter for <code>mysql.gtid_slave_pos.server_id</code>.
     */
    public void setServerId(UInteger value) {
        set(2, value);
    }

    /**
     * Getter for <code>mysql.gtid_slave_pos.server_id</code>.
     */
    public UInteger getServerId() {
        return (UInteger) get(2);
    }

    /**
     * Setter for <code>mysql.gtid_slave_pos.seq_no</code>.
     */
    public void setSeqNo(ULong value) {
        set(3, value);
    }

    /**
     * Getter for <code>mysql.gtid_slave_pos.seq_no</code>.
     */
    public ULong getSeqNo() {
        return (ULong) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record2<UInteger, ULong> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<UInteger, ULong, UInteger, ULong> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<UInteger, ULong, UInteger, ULong> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<UInteger> field1() {
        return GtidSlavePos.GTID_SLAVE_POS.DOMAIN_ID;
    }

    @Override
    public Field<ULong> field2() {
        return GtidSlavePos.GTID_SLAVE_POS.SUB_ID;
    }

    @Override
    public Field<UInteger> field3() {
        return GtidSlavePos.GTID_SLAVE_POS.SERVER_ID;
    }

    @Override
    public Field<ULong> field4() {
        return GtidSlavePos.GTID_SLAVE_POS.SEQ_NO;
    }

    @Override
    public UInteger component1() {
        return getDomainId();
    }

    @Override
    public ULong component2() {
        return getSubId();
    }

    @Override
    public UInteger component3() {
        return getServerId();
    }

    @Override
    public ULong component4() {
        return getSeqNo();
    }

    @Override
    public UInteger value1() {
        return getDomainId();
    }

    @Override
    public ULong value2() {
        return getSubId();
    }

    @Override
    public UInteger value3() {
        return getServerId();
    }

    @Override
    public ULong value4() {
        return getSeqNo();
    }

    @Override
    public GtidSlavePosRecord value1(UInteger value) {
        setDomainId(value);
        return this;
    }

    @Override
    public GtidSlavePosRecord value2(ULong value) {
        setSubId(value);
        return this;
    }

    @Override
    public GtidSlavePosRecord value3(UInteger value) {
        setServerId(value);
        return this;
    }

    @Override
    public GtidSlavePosRecord value4(ULong value) {
        setSeqNo(value);
        return this;
    }

    @Override
    public GtidSlavePosRecord values(UInteger value1, ULong value2, UInteger value3, ULong value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached GtidSlavePosRecord
     */
    public GtidSlavePosRecord() {
        super(GtidSlavePos.GTID_SLAVE_POS);
    }

    /**
     * Create a detached, initialised GtidSlavePosRecord
     */
    public GtidSlavePosRecord(UInteger domainId, ULong subId, UInteger serverId, ULong seqNo) {
        super(GtidSlavePos.GTID_SLAVE_POS);

        set(0, domainId);
        set(1, subId);
        set(2, serverId);
        set(3, seqNo);
    }
}
