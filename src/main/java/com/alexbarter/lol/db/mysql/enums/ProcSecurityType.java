/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.enums;


import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum ProcSecurityType implements EnumType {

    INVOKER("INVOKER"),

    DEFINER("DEFINER");

    private final String literal;

    private ProcSecurityType(String literal) {
        this.literal = literal;
    }

    @Override
    public Catalog getCatalog() {
        return null;
    }

    @Override
    public Schema getSchema() {
        return null;
    }

    @Override
    public String getName() {
        return "proc_security_type";
    }

    @Override
    public String getLiteral() {
        return literal;
    }
}
