/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.tables.daos;


import com.alexbarter.lol.db.mysql.tables.TableStats;
import com.alexbarter.lol.db.mysql.tables.records.TableStatsRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.Record2;
import org.jooq.impl.DAOImpl;
import org.jooq.types.ULong;


/**
 * Statistics on Tables
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TableStatsDao extends DAOImpl<TableStatsRecord, com.alexbarter.lol.db.mysql.tables.pojos.TableStats, Record2<String, String>> {

    /**
     * Create a new TableStatsDao without any configuration
     */
    public TableStatsDao() {
        super(TableStats.TABLE_STATS, com.alexbarter.lol.db.mysql.tables.pojos.TableStats.class);
    }

    /**
     * Create a new TableStatsDao with an attached configuration
     */
    public TableStatsDao(Configuration configuration) {
        super(TableStats.TABLE_STATS, com.alexbarter.lol.db.mysql.tables.pojos.TableStats.class, configuration);
    }

    @Override
    public Record2<String, String> getId(com.alexbarter.lol.db.mysql.tables.pojos.TableStats object) {
        return compositeKeyRecord(object.getDbName(), object.getTableName());
    }

    /**
     * Fetch records that have <code>db_name BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TableStats> fetchRangeOfDbName(String lowerInclusive, String upperInclusive) {
        return fetchRange(TableStats.TABLE_STATS.DB_NAME, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>db_name IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TableStats> fetchByDbName(String... values) {
        return fetch(TableStats.TABLE_STATS.DB_NAME, values);
    }

    /**
     * Fetch records that have <code>table_name BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TableStats> fetchRangeOfTableName(String lowerInclusive, String upperInclusive) {
        return fetchRange(TableStats.TABLE_STATS.TABLE_NAME, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>table_name IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TableStats> fetchByTableName(String... values) {
        return fetch(TableStats.TABLE_STATS.TABLE_NAME, values);
    }

    /**
     * Fetch records that have <code>cardinality BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TableStats> fetchRangeOfCardinality(ULong lowerInclusive, ULong upperInclusive) {
        return fetchRange(TableStats.TABLE_STATS.CARDINALITY, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>cardinality IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TableStats> fetchByCardinality(ULong... values) {
        return fetch(TableStats.TABLE_STATS.CARDINALITY, values);
    }
}
