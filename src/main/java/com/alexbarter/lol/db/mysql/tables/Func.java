/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.tables;


import com.alexbarter.lol.db.mysql.Indexes;
import com.alexbarter.lol.db.mysql.Keys;
import com.alexbarter.lol.db.mysql.Mysql;
import com.alexbarter.lol.db.mysql.enums.FuncType;
import com.alexbarter.lol.db.mysql.tables.records.FuncRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row4;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * User defined functions
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Func extends TableImpl<FuncRecord> {

    private static final long serialVersionUID = -848748393;

    /**
     * The reference instance of <code>mysql.func</code>
     */
    public static final Func FUNC = new Func();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<FuncRecord> getRecordType() {
        return FuncRecord.class;
    }

    /**
     * The column <code>mysql.func.name</code>.
     */
    public final TableField<FuncRecord, String> NAME = createField(DSL.name("name"), org.jooq.impl.SQLDataType.CHAR(64).nullable(false).defaultValue(org.jooq.impl.DSL.field("''", org.jooq.impl.SQLDataType.CHAR)), this, "");

    /**
     * The column <code>mysql.func.ret</code>.
     */
    public final TableField<FuncRecord, Byte> RET = createField(DSL.name("ret"), org.jooq.impl.SQLDataType.TINYINT.nullable(false).defaultValue(org.jooq.impl.DSL.field("0", org.jooq.impl.SQLDataType.TINYINT)), this, "");

    /**
     * The column <code>mysql.func.dl</code>.
     */
    public final TableField<FuncRecord, String> DL = createField(DSL.name("dl"), org.jooq.impl.SQLDataType.CHAR(128).nullable(false).defaultValue(org.jooq.impl.DSL.field("''", org.jooq.impl.SQLDataType.CHAR)), this, "");

    /**
     * The column <code>mysql.func.type</code>.
     */
    public final TableField<FuncRecord, FuncType> TYPE = createField(DSL.name("type"), org.jooq.impl.SQLDataType.VARCHAR(9).nullable(false).asEnumDataType(com.alexbarter.lol.db.mysql.enums.FuncType.class), this, "");

    /**
     * Create a <code>mysql.func</code> table reference
     */
    public Func() {
        this(DSL.name("func"), null);
    }

    /**
     * Create an aliased <code>mysql.func</code> table reference
     */
    public Func(String alias) {
        this(DSL.name(alias), FUNC);
    }

    /**
     * Create an aliased <code>mysql.func</code> table reference
     */
    public Func(Name alias) {
        this(alias, FUNC);
    }

    private Func(Name alias, Table<FuncRecord> aliased) {
        this(alias, aliased, null);
    }

    private Func(Name alias, Table<FuncRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment("User defined functions"));
    }

    public <O extends Record> Func(Table<O> child, ForeignKey<O, FuncRecord> key) {
        super(child, key, FUNC);
    }

    @Override
    public Schema getSchema() {
        return Mysql.MYSQL;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.FUNC_PRIMARY);
    }

    @Override
    public UniqueKey<FuncRecord> getPrimaryKey() {
        return Keys.KEY_FUNC_PRIMARY;
    }

    @Override
    public List<UniqueKey<FuncRecord>> getKeys() {
        return Arrays.<UniqueKey<FuncRecord>>asList(Keys.KEY_FUNC_PRIMARY);
    }

    @Override
    public Func as(String alias) {
        return new Func(DSL.name(alias), this);
    }

    @Override
    public Func as(Name alias) {
        return new Func(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Func rename(String name) {
        return new Func(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Func rename(Name name) {
        return new Func(name, null);
    }

    // -------------------------------------------------------------------------
    // Row4 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row4<String, Byte, String, FuncType> fieldsRow() {
        return (Row4) super.fieldsRow();
    }
}
