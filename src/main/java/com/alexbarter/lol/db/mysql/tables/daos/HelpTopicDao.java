/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.tables.daos;


import com.alexbarter.lol.db.mysql.tables.HelpTopic;
import com.alexbarter.lol.db.mysql.tables.records.HelpTopicRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;
import org.jooq.types.UInteger;
import org.jooq.types.UShort;


/**
 * help topics
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class HelpTopicDao extends DAOImpl<HelpTopicRecord, com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic, UInteger> {

    /**
     * Create a new HelpTopicDao without any configuration
     */
    public HelpTopicDao() {
        super(HelpTopic.HELP_TOPIC, com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic.class);
    }

    /**
     * Create a new HelpTopicDao with an attached configuration
     */
    public HelpTopicDao(Configuration configuration) {
        super(HelpTopic.HELP_TOPIC, com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic.class, configuration);
    }

    @Override
    public UInteger getId(com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic object) {
        return object.getHelpTopicId();
    }

    /**
     * Fetch records that have <code>help_topic_id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchRangeOfHelpTopicId(UInteger lowerInclusive, UInteger upperInclusive) {
        return fetchRange(HelpTopic.HELP_TOPIC.HELP_TOPIC_ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>help_topic_id IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchByHelpTopicId(UInteger... values) {
        return fetch(HelpTopic.HELP_TOPIC.HELP_TOPIC_ID, values);
    }

    /**
     * Fetch a unique record that has <code>help_topic_id = value</code>
     */
    public com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic fetchOneByHelpTopicId(UInteger value) {
        return fetchOne(HelpTopic.HELP_TOPIC.HELP_TOPIC_ID, value);
    }

    /**
     * Fetch records that have <code>name BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchRangeOfName(String lowerInclusive, String upperInclusive) {
        return fetchRange(HelpTopic.HELP_TOPIC.NAME, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>name IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchByName(String... values) {
        return fetch(HelpTopic.HELP_TOPIC.NAME, values);
    }

    /**
     * Fetch a unique record that has <code>name = value</code>
     */
    public com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic fetchOneByName(String value) {
        return fetchOne(HelpTopic.HELP_TOPIC.NAME, value);
    }

    /**
     * Fetch records that have <code>help_category_id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchRangeOfHelpCategoryId(UShort lowerInclusive, UShort upperInclusive) {
        return fetchRange(HelpTopic.HELP_TOPIC.HELP_CATEGORY_ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>help_category_id IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchByHelpCategoryId(UShort... values) {
        return fetch(HelpTopic.HELP_TOPIC.HELP_CATEGORY_ID, values);
    }

    /**
     * Fetch records that have <code>description BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchRangeOfDescription(String lowerInclusive, String upperInclusive) {
        return fetchRange(HelpTopic.HELP_TOPIC.DESCRIPTION, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>description IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchByDescription(String... values) {
        return fetch(HelpTopic.HELP_TOPIC.DESCRIPTION, values);
    }

    /**
     * Fetch records that have <code>example BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchRangeOfExample(String lowerInclusive, String upperInclusive) {
        return fetchRange(HelpTopic.HELP_TOPIC.EXAMPLE, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>example IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchByExample(String... values) {
        return fetch(HelpTopic.HELP_TOPIC.EXAMPLE, values);
    }

    /**
     * Fetch records that have <code>url BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchRangeOfUrl(String lowerInclusive, String upperInclusive) {
        return fetchRange(HelpTopic.HELP_TOPIC.URL, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>url IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.HelpTopic> fetchByUrl(String... values) {
        return fetch(HelpTopic.HELP_TOPIC.URL, values);
    }
}
