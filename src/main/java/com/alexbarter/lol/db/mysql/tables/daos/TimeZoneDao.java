/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.tables.daos;


import com.alexbarter.lol.db.mysql.enums.TimeZoneUseLeapSeconds;
import com.alexbarter.lol.db.mysql.tables.TimeZone;
import com.alexbarter.lol.db.mysql.tables.records.TimeZoneRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;
import org.jooq.types.UInteger;


/**
 * Time zones
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TimeZoneDao extends DAOImpl<TimeZoneRecord, com.alexbarter.lol.db.mysql.tables.pojos.TimeZone, UInteger> {

    /**
     * Create a new TimeZoneDao without any configuration
     */
    public TimeZoneDao() {
        super(TimeZone.TIME_ZONE, com.alexbarter.lol.db.mysql.tables.pojos.TimeZone.class);
    }

    /**
     * Create a new TimeZoneDao with an attached configuration
     */
    public TimeZoneDao(Configuration configuration) {
        super(TimeZone.TIME_ZONE, com.alexbarter.lol.db.mysql.tables.pojos.TimeZone.class, configuration);
    }

    @Override
    public UInteger getId(com.alexbarter.lol.db.mysql.tables.pojos.TimeZone object) {
        return object.getTimeZoneId();
    }

    /**
     * Fetch records that have <code>Time_zone_id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZone> fetchRangeOfTimeZoneId(UInteger lowerInclusive, UInteger upperInclusive) {
        return fetchRange(TimeZone.TIME_ZONE.TIME_ZONE_ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Time_zone_id IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZone> fetchByTimeZoneId(UInteger... values) {
        return fetch(TimeZone.TIME_ZONE.TIME_ZONE_ID, values);
    }

    /**
     * Fetch a unique record that has <code>Time_zone_id = value</code>
     */
    public com.alexbarter.lol.db.mysql.tables.pojos.TimeZone fetchOneByTimeZoneId(UInteger value) {
        return fetchOne(TimeZone.TIME_ZONE.TIME_ZONE_ID, value);
    }

    /**
     * Fetch records that have <code>Use_leap_seconds BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZone> fetchRangeOfUseLeapSeconds(TimeZoneUseLeapSeconds lowerInclusive, TimeZoneUseLeapSeconds upperInclusive) {
        return fetchRange(TimeZone.TIME_ZONE.USE_LEAP_SECONDS, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Use_leap_seconds IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZone> fetchByUseLeapSeconds(TimeZoneUseLeapSeconds... values) {
        return fetch(TimeZone.TIME_ZONE.USE_LEAP_SECONDS, values);
    }
}
