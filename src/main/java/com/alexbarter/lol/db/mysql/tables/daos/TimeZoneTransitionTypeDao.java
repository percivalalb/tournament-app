/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.mysql.tables.daos;


import com.alexbarter.lol.db.mysql.tables.TimeZoneTransitionType;
import com.alexbarter.lol.db.mysql.tables.records.TimeZoneTransitionTypeRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.Record2;
import org.jooq.impl.DAOImpl;
import org.jooq.types.UByte;
import org.jooq.types.UInteger;


/**
 * Time zone transition types
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TimeZoneTransitionTypeDao extends DAOImpl<TimeZoneTransitionTypeRecord, com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType, Record2<UInteger, UInteger>> {

    /**
     * Create a new TimeZoneTransitionTypeDao without any configuration
     */
    public TimeZoneTransitionTypeDao() {
        super(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE, com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType.class);
    }

    /**
     * Create a new TimeZoneTransitionTypeDao with an attached configuration
     */
    public TimeZoneTransitionTypeDao(Configuration configuration) {
        super(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE, com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType.class, configuration);
    }

    @Override
    public Record2<UInteger, UInteger> getId(com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType object) {
        return compositeKeyRecord(object.getTimeZoneId(), object.getTransitionTypeId());
    }

    /**
     * Fetch records that have <code>Time_zone_id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchRangeOfTimeZoneId(UInteger lowerInclusive, UInteger upperInclusive) {
        return fetchRange(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.TIME_ZONE_ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Time_zone_id IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchByTimeZoneId(UInteger... values) {
        return fetch(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.TIME_ZONE_ID, values);
    }

    /**
     * Fetch records that have <code>Transition_type_id BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchRangeOfTransitionTypeId(UInteger lowerInclusive, UInteger upperInclusive) {
        return fetchRange(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.TRANSITION_TYPE_ID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Transition_type_id IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchByTransitionTypeId(UInteger... values) {
        return fetch(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.TRANSITION_TYPE_ID, values);
    }

    /**
     * Fetch records that have <code>Offset BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchRangeOfOffset(Integer lowerInclusive, Integer upperInclusive) {
        return fetchRange(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.OFFSET, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Offset IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchByOffset(Integer... values) {
        return fetch(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.OFFSET, values);
    }

    /**
     * Fetch records that have <code>Is_DST BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchRangeOfIsDst(UByte lowerInclusive, UByte upperInclusive) {
        return fetchRange(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.IS_DST, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Is_DST IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchByIsDst(UByte... values) {
        return fetch(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.IS_DST, values);
    }

    /**
     * Fetch records that have <code>Abbreviation BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchRangeOfAbbreviation(String lowerInclusive, String upperInclusive) {
        return fetchRange(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.ABBREVIATION, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>Abbreviation IN (values)</code>
     */
    public List<com.alexbarter.lol.db.mysql.tables.pojos.TimeZoneTransitionType> fetchByAbbreviation(String... values) {
        return fetch(TimeZoneTransitionType.TIME_ZONE_TRANSITION_TYPE.ABBREVIATION, values);
    }
}
