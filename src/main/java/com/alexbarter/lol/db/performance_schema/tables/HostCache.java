/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.performance_schema.tables;


import com.alexbarter.lol.db.performance_schema.PerformanceSchema;
import com.alexbarter.lol.db.performance_schema.enums.HostCacheHostValidated;
import com.alexbarter.lol.db.performance_schema.tables.records.HostCacheRecord;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class HostCache extends TableImpl<HostCacheRecord> {

    private static final long serialVersionUID = 1457002718;

    /**
     * The reference instance of <code>performance_schema.host_cache</code>
     */
    public static final HostCache HOST_CACHE = new HostCache();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<HostCacheRecord> getRecordType() {
        return HostCacheRecord.class;
    }

    /**
     * The column <code>performance_schema.host_cache.IP</code>.
     */
    public final TableField<HostCacheRecord, String> IP = createField(DSL.name("IP"), org.jooq.impl.SQLDataType.VARCHAR(64).nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.HOST</code>.
     */
    public final TableField<HostCacheRecord, String> HOST = createField(DSL.name("HOST"), org.jooq.impl.SQLDataType.VARCHAR(255).defaultValue(org.jooq.impl.DSL.field("NULL", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * The column <code>performance_schema.host_cache.HOST_VALIDATED</code>.
     */
    public final TableField<HostCacheRecord, HostCacheHostValidated> HOST_VALIDATED = createField(DSL.name("HOST_VALIDATED"), org.jooq.impl.SQLDataType.VARCHAR(3).nullable(false).asEnumDataType(com.alexbarter.lol.db.performance_schema.enums.HostCacheHostValidated.class), this, "");

    /**
     * The column <code>performance_schema.host_cache.SUM_CONNECT_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> SUM_CONNECT_ERRORS = createField(DSL.name("SUM_CONNECT_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_HOST_BLOCKED_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_HOST_BLOCKED_ERRORS = createField(DSL.name("COUNT_HOST_BLOCKED_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_NAMEINFO_TRANSIENT_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_NAMEINFO_TRANSIENT_ERRORS = createField(DSL.name("COUNT_NAMEINFO_TRANSIENT_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_NAMEINFO_PERMANENT_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_NAMEINFO_PERMANENT_ERRORS = createField(DSL.name("COUNT_NAMEINFO_PERMANENT_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_FORMAT_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_FORMAT_ERRORS = createField(DSL.name("COUNT_FORMAT_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_ADDRINFO_TRANSIENT_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_ADDRINFO_TRANSIENT_ERRORS = createField(DSL.name("COUNT_ADDRINFO_TRANSIENT_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_ADDRINFO_PERMANENT_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_ADDRINFO_PERMANENT_ERRORS = createField(DSL.name("COUNT_ADDRINFO_PERMANENT_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_FCRDNS_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_FCRDNS_ERRORS = createField(DSL.name("COUNT_FCRDNS_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_HOST_ACL_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_HOST_ACL_ERRORS = createField(DSL.name("COUNT_HOST_ACL_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_NO_AUTH_PLUGIN_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_NO_AUTH_PLUGIN_ERRORS = createField(DSL.name("COUNT_NO_AUTH_PLUGIN_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_AUTH_PLUGIN_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_AUTH_PLUGIN_ERRORS = createField(DSL.name("COUNT_AUTH_PLUGIN_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_HANDSHAKE_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_HANDSHAKE_ERRORS = createField(DSL.name("COUNT_HANDSHAKE_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_PROXY_USER_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_PROXY_USER_ERRORS = createField(DSL.name("COUNT_PROXY_USER_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_PROXY_USER_ACL_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_PROXY_USER_ACL_ERRORS = createField(DSL.name("COUNT_PROXY_USER_ACL_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_AUTHENTICATION_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_AUTHENTICATION_ERRORS = createField(DSL.name("COUNT_AUTHENTICATION_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_SSL_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_SSL_ERRORS = createField(DSL.name("COUNT_SSL_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_MAX_USER_CONNECTIONS_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_MAX_USER_CONNECTIONS_ERRORS = createField(DSL.name("COUNT_MAX_USER_CONNECTIONS_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_MAX_USER_CONNECTIONS_PER_HOUR_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_MAX_USER_CONNECTIONS_PER_HOUR_ERRORS = createField(DSL.name("COUNT_MAX_USER_CONNECTIONS_PER_HOUR_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_DEFAULT_DATABASE_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_DEFAULT_DATABASE_ERRORS = createField(DSL.name("COUNT_DEFAULT_DATABASE_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_INIT_CONNECT_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_INIT_CONNECT_ERRORS = createField(DSL.name("COUNT_INIT_CONNECT_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_LOCAL_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_LOCAL_ERRORS = createField(DSL.name("COUNT_LOCAL_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.COUNT_UNKNOWN_ERRORS</code>.
     */
    public final TableField<HostCacheRecord, Long> COUNT_UNKNOWN_ERRORS = createField(DSL.name("COUNT_UNKNOWN_ERRORS"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>performance_schema.host_cache.FIRST_SEEN</code>.
     */
    public final TableField<HostCacheRecord, Timestamp> FIRST_SEEN = createField(DSL.name("FIRST_SEEN"), org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.field("'0000-00-00 00:00:00'", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>performance_schema.host_cache.LAST_SEEN</code>.
     */
    public final TableField<HostCacheRecord, Timestamp> LAST_SEEN = createField(DSL.name("LAST_SEEN"), org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.field("'0000-00-00 00:00:00'", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>performance_schema.host_cache.FIRST_ERROR_SEEN</code>.
     */
    public final TableField<HostCacheRecord, Timestamp> FIRST_ERROR_SEEN = createField(DSL.name("FIRST_ERROR_SEEN"), org.jooq.impl.SQLDataType.TIMESTAMP.defaultValue(org.jooq.impl.DSL.field("'0000-00-00 00:00:00'", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>performance_schema.host_cache.LAST_ERROR_SEEN</code>.
     */
    public final TableField<HostCacheRecord, Timestamp> LAST_ERROR_SEEN = createField(DSL.name("LAST_ERROR_SEEN"), org.jooq.impl.SQLDataType.TIMESTAMP.defaultValue(org.jooq.impl.DSL.field("'0000-00-00 00:00:00'", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * Create a <code>performance_schema.host_cache</code> table reference
     */
    public HostCache() {
        this(DSL.name("host_cache"), null);
    }

    /**
     * Create an aliased <code>performance_schema.host_cache</code> table reference
     */
    public HostCache(String alias) {
        this(DSL.name(alias), HOST_CACHE);
    }

    /**
     * Create an aliased <code>performance_schema.host_cache</code> table reference
     */
    public HostCache(Name alias) {
        this(alias, HOST_CACHE);
    }

    private HostCache(Name alias, Table<HostCacheRecord> aliased) {
        this(alias, aliased, null);
    }

    private HostCache(Name alias, Table<HostCacheRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> HostCache(Table<O> child, ForeignKey<O, HostCacheRecord> key) {
        super(child, key, HOST_CACHE);
    }

    @Override
    public Schema getSchema() {
        return PerformanceSchema.PERFORMANCE_SCHEMA;
    }

    @Override
    public HostCache as(String alias) {
        return new HostCache(DSL.name(alias), this);
    }

    @Override
    public HostCache as(Name alias) {
        return new HostCache(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public HostCache rename(String name) {
        return new HostCache(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public HostCache rename(Name name) {
        return new HostCache(name, null);
    }
}
