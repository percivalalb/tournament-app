/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.performance_schema.tables.pojos;


import com.alexbarter.lol.db.performance_schema.enums.EventsStagesHistoryLongNestingEventType;

import java.io.Serializable;

import javax.annotation.Generated;

import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class EventsStagesHistoryLong implements Serializable {

    private static final long serialVersionUID = -1567813760;

    private ULong                                   threadId;
    private ULong                                   eventId;
    private ULong                                   endEventId;
    private String                                  eventName;
    private String                                  source;
    private ULong                                   timerStart;
    private ULong                                   timerEnd;
    private ULong                                   timerWait;
    private ULong                                   nestingEventId;
    private EventsStagesHistoryLongNestingEventType nestingEventType;

    public EventsStagesHistoryLong() {}

    public EventsStagesHistoryLong(EventsStagesHistoryLong value) {
        this.threadId = value.threadId;
        this.eventId = value.eventId;
        this.endEventId = value.endEventId;
        this.eventName = value.eventName;
        this.source = value.source;
        this.timerStart = value.timerStart;
        this.timerEnd = value.timerEnd;
        this.timerWait = value.timerWait;
        this.nestingEventId = value.nestingEventId;
        this.nestingEventType = value.nestingEventType;
    }

    public EventsStagesHistoryLong(
        ULong                                   threadId,
        ULong                                   eventId,
        ULong                                   endEventId,
        String                                  eventName,
        String                                  source,
        ULong                                   timerStart,
        ULong                                   timerEnd,
        ULong                                   timerWait,
        ULong                                   nestingEventId,
        EventsStagesHistoryLongNestingEventType nestingEventType
    ) {
        this.threadId = threadId;
        this.eventId = eventId;
        this.endEventId = endEventId;
        this.eventName = eventName;
        this.source = source;
        this.timerStart = timerStart;
        this.timerEnd = timerEnd;
        this.timerWait = timerWait;
        this.nestingEventId = nestingEventId;
        this.nestingEventType = nestingEventType;
    }

    public ULong getThreadId() {
        return this.threadId;
    }

    public void setThreadId(ULong threadId) {
        this.threadId = threadId;
    }

    public ULong getEventId() {
        return this.eventId;
    }

    public void setEventId(ULong eventId) {
        this.eventId = eventId;
    }

    public ULong getEndEventId() {
        return this.endEventId;
    }

    public void setEndEventId(ULong endEventId) {
        this.endEventId = endEventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public ULong getTimerStart() {
        return this.timerStart;
    }

    public void setTimerStart(ULong timerStart) {
        this.timerStart = timerStart;
    }

    public ULong getTimerEnd() {
        return this.timerEnd;
    }

    public void setTimerEnd(ULong timerEnd) {
        this.timerEnd = timerEnd;
    }

    public ULong getTimerWait() {
        return this.timerWait;
    }

    public void setTimerWait(ULong timerWait) {
        this.timerWait = timerWait;
    }

    public ULong getNestingEventId() {
        return this.nestingEventId;
    }

    public void setNestingEventId(ULong nestingEventId) {
        this.nestingEventId = nestingEventId;
    }

    public EventsStagesHistoryLongNestingEventType getNestingEventType() {
        return this.nestingEventType;
    }

    public void setNestingEventType(EventsStagesHistoryLongNestingEventType nestingEventType) {
        this.nestingEventType = nestingEventType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("EventsStagesHistoryLong (");

        sb.append(threadId);
        sb.append(", ").append(eventId);
        sb.append(", ").append(endEventId);
        sb.append(", ").append(eventName);
        sb.append(", ").append(source);
        sb.append(", ").append(timerStart);
        sb.append(", ").append(timerEnd);
        sb.append(", ").append(timerWait);
        sb.append(", ").append(nestingEventId);
        sb.append(", ").append(nestingEventType);

        sb.append(")");
        return sb.toString();
    }
}
