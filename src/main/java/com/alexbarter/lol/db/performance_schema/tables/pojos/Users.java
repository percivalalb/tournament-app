/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.performance_schema.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Users implements Serializable {

    private static final long serialVersionUID = 521664837;

    private String user;
    private Long   currentConnections;
    private Long   totalConnections;

    public Users() {}

    public Users(Users value) {
        this.user = value.user;
        this.currentConnections = value.currentConnections;
        this.totalConnections = value.totalConnections;
    }

    public Users(
        String user,
        Long   currentConnections,
        Long   totalConnections
    ) {
        this.user = user;
        this.currentConnections = currentConnections;
        this.totalConnections = totalConnections;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getCurrentConnections() {
        return this.currentConnections;
    }

    public void setCurrentConnections(Long currentConnections) {
        this.currentConnections = currentConnections;
    }

    public Long getTotalConnections() {
        return this.totalConnections;
    }

    public void setTotalConnections(Long totalConnections) {
        this.totalConnections = totalConnections;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Users (");

        sb.append(user);
        sb.append(", ").append(currentConnections);
        sb.append(", ").append(totalConnections);

        sb.append(")");
        return sb.toString();
    }
}
