/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.performance_schema.tables;


import com.alexbarter.lol.db.performance_schema.PerformanceSchema;
import com.alexbarter.lol.db.performance_schema.enums.SetupInstrumentsEnabled;
import com.alexbarter.lol.db.performance_schema.enums.SetupInstrumentsTimed;
import com.alexbarter.lol.db.performance_schema.tables.records.SetupInstrumentsRecord;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row3;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class SetupInstruments extends TableImpl<SetupInstrumentsRecord> {

    private static final long serialVersionUID = 1295589546;

    /**
     * The reference instance of <code>performance_schema.setup_instruments</code>
     */
    public static final SetupInstruments SETUP_INSTRUMENTS = new SetupInstruments();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<SetupInstrumentsRecord> getRecordType() {
        return SetupInstrumentsRecord.class;
    }

    /**
     * The column <code>performance_schema.setup_instruments.NAME</code>.
     */
    public final TableField<SetupInstrumentsRecord, String> NAME = createField(DSL.name("NAME"), org.jooq.impl.SQLDataType.VARCHAR(128).nullable(false), this, "");

    /**
     * The column <code>performance_schema.setup_instruments.ENABLED</code>.
     */
    public final TableField<SetupInstrumentsRecord, SetupInstrumentsEnabled> ENABLED = createField(DSL.name("ENABLED"), org.jooq.impl.SQLDataType.VARCHAR(3).nullable(false).asEnumDataType(com.alexbarter.lol.db.performance_schema.enums.SetupInstrumentsEnabled.class), this, "");

    /**
     * The column <code>performance_schema.setup_instruments.TIMED</code>.
     */
    public final TableField<SetupInstrumentsRecord, SetupInstrumentsTimed> TIMED = createField(DSL.name("TIMED"), org.jooq.impl.SQLDataType.VARCHAR(3).nullable(false).asEnumDataType(com.alexbarter.lol.db.performance_schema.enums.SetupInstrumentsTimed.class), this, "");

    /**
     * Create a <code>performance_schema.setup_instruments</code> table reference
     */
    public SetupInstruments() {
        this(DSL.name("setup_instruments"), null);
    }

    /**
     * Create an aliased <code>performance_schema.setup_instruments</code> table reference
     */
    public SetupInstruments(String alias) {
        this(DSL.name(alias), SETUP_INSTRUMENTS);
    }

    /**
     * Create an aliased <code>performance_schema.setup_instruments</code> table reference
     */
    public SetupInstruments(Name alias) {
        this(alias, SETUP_INSTRUMENTS);
    }

    private SetupInstruments(Name alias, Table<SetupInstrumentsRecord> aliased) {
        this(alias, aliased, null);
    }

    private SetupInstruments(Name alias, Table<SetupInstrumentsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> SetupInstruments(Table<O> child, ForeignKey<O, SetupInstrumentsRecord> key) {
        super(child, key, SETUP_INSTRUMENTS);
    }

    @Override
    public Schema getSchema() {
        return PerformanceSchema.PERFORMANCE_SCHEMA;
    }

    @Override
    public SetupInstruments as(String alias) {
        return new SetupInstruments(DSL.name(alias), this);
    }

    @Override
    public SetupInstruments as(Name alias) {
        return new SetupInstruments(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public SetupInstruments rename(String name) {
        return new SetupInstruments(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public SetupInstruments rename(Name name) {
        return new SetupInstruments(name, null);
    }

    // -------------------------------------------------------------------------
    // Row3 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row3<String, SetupInstrumentsEnabled, SetupInstrumentsTimed> fieldsRow() {
        return (Row3) super.fieldsRow();
    }
}
