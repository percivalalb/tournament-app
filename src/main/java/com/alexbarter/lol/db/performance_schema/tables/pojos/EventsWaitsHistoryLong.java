/*
 * This file is generated by jOOQ.
 */
package com.alexbarter.lol.db.performance_schema.tables.pojos;


import com.alexbarter.lol.db.performance_schema.enums.EventsWaitsHistoryLongNestingEventType;

import java.io.Serializable;

import javax.annotation.Generated;

import org.jooq.types.UInteger;
import org.jooq.types.ULong;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.0"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class EventsWaitsHistoryLong implements Serializable {

    private static final long serialVersionUID = -591527317;

    private ULong                                  threadId;
    private ULong                                  eventId;
    private ULong                                  endEventId;
    private String                                 eventName;
    private String                                 source;
    private ULong                                  timerStart;
    private ULong                                  timerEnd;
    private ULong                                  timerWait;
    private UInteger                               spins;
    private String                                 objectSchema;
    private String                                 objectName;
    private String                                 indexName;
    private String                                 objectType;
    private ULong                                  objectInstanceBegin;
    private ULong                                  nestingEventId;
    private EventsWaitsHistoryLongNestingEventType nestingEventType;
    private String                                 operation;
    private Long                                   numberOfBytes;
    private UInteger                               flags;

    public EventsWaitsHistoryLong() {}

    public EventsWaitsHistoryLong(EventsWaitsHistoryLong value) {
        this.threadId = value.threadId;
        this.eventId = value.eventId;
        this.endEventId = value.endEventId;
        this.eventName = value.eventName;
        this.source = value.source;
        this.timerStart = value.timerStart;
        this.timerEnd = value.timerEnd;
        this.timerWait = value.timerWait;
        this.spins = value.spins;
        this.objectSchema = value.objectSchema;
        this.objectName = value.objectName;
        this.indexName = value.indexName;
        this.objectType = value.objectType;
        this.objectInstanceBegin = value.objectInstanceBegin;
        this.nestingEventId = value.nestingEventId;
        this.nestingEventType = value.nestingEventType;
        this.operation = value.operation;
        this.numberOfBytes = value.numberOfBytes;
        this.flags = value.flags;
    }

    public EventsWaitsHistoryLong(
        ULong                                  threadId,
        ULong                                  eventId,
        ULong                                  endEventId,
        String                                 eventName,
        String                                 source,
        ULong                                  timerStart,
        ULong                                  timerEnd,
        ULong                                  timerWait,
        UInteger                               spins,
        String                                 objectSchema,
        String                                 objectName,
        String                                 indexName,
        String                                 objectType,
        ULong                                  objectInstanceBegin,
        ULong                                  nestingEventId,
        EventsWaitsHistoryLongNestingEventType nestingEventType,
        String                                 operation,
        Long                                   numberOfBytes,
        UInteger                               flags
    ) {
        this.threadId = threadId;
        this.eventId = eventId;
        this.endEventId = endEventId;
        this.eventName = eventName;
        this.source = source;
        this.timerStart = timerStart;
        this.timerEnd = timerEnd;
        this.timerWait = timerWait;
        this.spins = spins;
        this.objectSchema = objectSchema;
        this.objectName = objectName;
        this.indexName = indexName;
        this.objectType = objectType;
        this.objectInstanceBegin = objectInstanceBegin;
        this.nestingEventId = nestingEventId;
        this.nestingEventType = nestingEventType;
        this.operation = operation;
        this.numberOfBytes = numberOfBytes;
        this.flags = flags;
    }

    public ULong getThreadId() {
        return this.threadId;
    }

    public void setThreadId(ULong threadId) {
        this.threadId = threadId;
    }

    public ULong getEventId() {
        return this.eventId;
    }

    public void setEventId(ULong eventId) {
        this.eventId = eventId;
    }

    public ULong getEndEventId() {
        return this.endEventId;
    }

    public void setEndEventId(ULong endEventId) {
        this.endEventId = endEventId;
    }

    public String getEventName() {
        return this.eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public ULong getTimerStart() {
        return this.timerStart;
    }

    public void setTimerStart(ULong timerStart) {
        this.timerStart = timerStart;
    }

    public ULong getTimerEnd() {
        return this.timerEnd;
    }

    public void setTimerEnd(ULong timerEnd) {
        this.timerEnd = timerEnd;
    }

    public ULong getTimerWait() {
        return this.timerWait;
    }

    public void setTimerWait(ULong timerWait) {
        this.timerWait = timerWait;
    }

    public UInteger getSpins() {
        return this.spins;
    }

    public void setSpins(UInteger spins) {
        this.spins = spins;
    }

    public String getObjectSchema() {
        return this.objectSchema;
    }

    public void setObjectSchema(String objectSchema) {
        this.objectSchema = objectSchema;
    }

    public String getObjectName() {
        return this.objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getIndexName() {
        return this.indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getObjectType() {
        return this.objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public ULong getObjectInstanceBegin() {
        return this.objectInstanceBegin;
    }

    public void setObjectInstanceBegin(ULong objectInstanceBegin) {
        this.objectInstanceBegin = objectInstanceBegin;
    }

    public ULong getNestingEventId() {
        return this.nestingEventId;
    }

    public void setNestingEventId(ULong nestingEventId) {
        this.nestingEventId = nestingEventId;
    }

    public EventsWaitsHistoryLongNestingEventType getNestingEventType() {
        return this.nestingEventType;
    }

    public void setNestingEventType(EventsWaitsHistoryLongNestingEventType nestingEventType) {
        this.nestingEventType = nestingEventType;
    }

    public String getOperation() {
        return this.operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Long getNumberOfBytes() {
        return this.numberOfBytes;
    }

    public void setNumberOfBytes(Long numberOfBytes) {
        this.numberOfBytes = numberOfBytes;
    }

    public UInteger getFlags() {
        return this.flags;
    }

    public void setFlags(UInteger flags) {
        this.flags = flags;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("EventsWaitsHistoryLong (");

        sb.append(threadId);
        sb.append(", ").append(eventId);
        sb.append(", ").append(endEventId);
        sb.append(", ").append(eventName);
        sb.append(", ").append(source);
        sb.append(", ").append(timerStart);
        sb.append(", ").append(timerEnd);
        sb.append(", ").append(timerWait);
        sb.append(", ").append(spins);
        sb.append(", ").append(objectSchema);
        sb.append(", ").append(objectName);
        sb.append(", ").append(indexName);
        sb.append(", ").append(objectType);
        sb.append(", ").append(objectInstanceBegin);
        sb.append(", ").append(nestingEventId);
        sb.append(", ").append(nestingEventType);
        sb.append(", ").append(operation);
        sb.append(", ").append(numberOfBytes);
        sb.append(", ").append(flags);

        sb.append(")");
        return sb.toString();
    }
}
