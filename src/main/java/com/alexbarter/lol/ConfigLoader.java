package com.alexbarter.lol;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class ConfigLoader {

	@SuppressWarnings("unchecked")
	public static <T> T load(String section) {
	    try (InputStream inputStream = Files.newInputStream(Paths.get("deploy", "config.yaml"))) {
    		Map<String, Object> map = (new Yaml()).load(inputStream);
    		return (T) map.get(section);
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
}
