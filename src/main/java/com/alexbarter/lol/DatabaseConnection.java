package com.alexbarter.lol;

import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.Select;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import com.alexbarter.lib.Utils;

public class DatabaseConnection implements AutoCloseable {

	private Connection connection;
	private DSLContext ctx;
	private Properties properties;

	public DatabaseConnection(String username, String password, String database) {
	    this.properties = new Properties();
	    if (username != null) {
	        this.properties.put("user", username);
        }
        if (password != null) {
            this.properties.put("password", password);
        }
        if (database != null) {
            this.properties.put("database", database);
        }
    }

	public DatabaseConnection(Properties properties, String database) {
        this.properties = properties;
        if (database != null) {
            this.properties.put("database", database);
        }
    }

	public DatabaseConnection(Properties properties) {
	    this.properties = properties;
	}

	public synchronized void connect() {
	    // Make sure connection is closed
	    Utils.tryClose(this.connection);
	    Utils.tryClose(this.ctx);

	    try {
            Class.forName("org.mariadb.jdbc.Driver");

            this.connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + this.properties.getProperty("database", ""), this.properties);
            this.ctx = DSL.using(this.connection);
            System.out.println("Connection is successful");
        } catch (Exception e) {
            throw new RuntimeException("Connection unsuccessful: " + e.getMessage());
        }
	}

	public <Q extends Query> Q getCtx(Q query) {
        return trySql(ctx -> {
            query.attach(ctx.configuration());
            query.execute();
            return query;
        });
    }

	public <Q extends Query> int execute(Q query) {
        return trySql(ctx -> {
            query.attach(ctx.configuration());
            return query.execute();
        });
    }

    public boolean fetchExists(Select<?> where) {
        return trySql(ctx -> {
            return ctx.fetchExists(where);
        });
    }

	public <T> T trySql(SQLSupplier<T> supp) {
	    RuntimeException lastExpection;
	    int attempts = 0;
	    do {
    	    try {
    	        return supp.accept(this.ctx);
            } catch (DataAccessException e) {
                lastExpection = e;

                SocketException ex = e.getCause(SocketException.class);
                if (ex != null) {
                    this.connect();
                } else {
                    throw e;
                }
            } finally {
                attempts++;
            }
	    } while (attempts < 2);

	    throw lastExpection;
	}

	@Override
	public void close() {
		Utils.tryClose(this.connection);
		Utils.tryClose(this.ctx);
	}

	@FunctionalInterface
	public interface SQLSupplier<T> {

        public T accept(DSLContext ctx) throws DataAccessException;

	}
}
