package com.alexbarter.lol;

import static spark.Spark.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.alexbarter.lib.Maps;
import com.alexbarter.lol.api.APIResponse;
import com.alexbarter.lol.api.APIUtils;
import com.alexbarter.lol.api.AuthentiationAPI;
import com.alexbarter.lol.api.TournamentAPI;
import com.alexbarter.lol.matchmaking.Game;
import com.alexbarter.lol.matchmaking.Tournament;

import net.rithms.riot.api.ApiConfig;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import spark.Service;

/**
 * This example demonstrates using the RiotApi to request summoner information for a given summoner name
 */
public class MainTest {

    public static DatabaseConnection dbh;
    public static RiotApi api;

	public static void main(String[] args) throws RiotApiException, SQLException, IOException {
		Map<String, String> databaseConfig = ConfigLoader.load("database");
        Map<String, String> riotConfig = ConfigLoader.load("riot");
        Map<String, String> httpsConfig = ConfigLoader.load("https");
		dbh = new DatabaseConnection(databaseConfig.get("username"), databaseConfig.get("password"), "lol");
		dbh.connect();

		boolean https = true;//!System.getProperty("os.name").contains("Windows");

		if (https) {
    		port(443);
            secure(httpsConfig.get("keystore"), httpsConfig.get("password"), null, null);

            // Starts a lightweight server running on port 80 (http)
            // that redirects to https (port 443)
            Service http = Service.ignite();
            http.port(80).threadPool(4, 4, -1);
            http.before(((request, response) -> {
                final String url = request.url();
                if (url.startsWith("http://")) {
                    final String[] split = url.split("http://");
                    response.redirect("https://" + split[1]);
                }
            }));
		} else {
		    port(80);
		}

	    staticFileLocation("/public");
	    staticFiles.expireTime(600);

		redirect.get("/lol", "/lol/welcome");
		redirect.get("/lol/", "/lol/register");
		redirect.get("/lol/", "/lol/register");

//		notFound((req, res) -> {
//		    Maps params = Maps.of(
//                    "title", "Page not found",
//                    "authed", AuthentiationAPI.isAuthenticated(req),
//                    "error", "Page not found");
//
//            return APIUtils.render(params.get(), "/templates/error.vm");
//        });
//
//		internalServerError((req, res) -> {
//            Maps params = Maps.of(
//                    "title", "Internal Server Error",
//                    "authed", AuthentiationAPI.isAuthenticated(req),
//                    "error", "Internal Server Error");
//
//            return APIUtils.render(params.get(), "/templates/error.vm");
//        });

		// Require authentication before these paths
        before("/lol/admin", AuthentiationAPI::accessAllowed);
        before("/lol/admin/*", AuthentiationAPI::accessAllowed);
        before("/lol/tournament/:tournament/admin", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/remove", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/add", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/deregister", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/create", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/recreate", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/copy-game", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/report-result", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/reset", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/start", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/toggle-playing", AuthentiationAPI::accessAllowed);
        before("/lol/api/tournament/toggle-signups", AuthentiationAPI::accessAllowed);

        get("/", (req, res) -> {
            Maps params = Maps.of()
                    .put("title", "Welcome")
                    .put("authed", AuthentiationAPI.isAuthenticated(req));

            return APIUtils.render(params.get(), "/templates/welcome.vm");
        });

		path("/lol", () -> {
		    path("/tournament", () -> {
		        before("/:tournament", (req, res) -> {
		            res.redirect("/lol/tournament/"+req.params("tournament")+"/register");
		        });

                get("/:tournament/register", (req, res) -> {
                    Tournament tournament = Tournament.get(MainTest.dbh, req.params("tournament"));

                    if (tournament == null) {
                        return APIResponse.create(req, res).setError("Tournament does not exist.");
                    }

                    Maps params = Maps.of()
                            .put("title", "Register")
                            .put("authed", AuthentiationAPI.isAuthenticated(req))
                            .put("tournament", tournament);

                    return APIUtils.render(params.get(), "/templates/register.vm");
                });

                get("/:tournament/view", (req, res) -> {
                    Tournament tournament = Tournament.get(MainTest.dbh, req.params("tournament"));

                    if (tournament == null) {
                        return APIResponse.create(req, res).setError("Tournament does not exist.");
                    }
                    Maps params = Maps.of()
                            .put("title", "View")
                            .put("authed", AuthentiationAPI.isAuthenticated(req))
                            .put("tournament", tournament);

                    return APIUtils.render(params.get(), "/templates/view.vm");
                });

                get("/:tournament/leaderboard", (req, res) -> {
                    Tournament tournament = Tournament.get(MainTest.dbh, req.params("tournament"));

                    if (tournament == null) {
                        return APIResponse.create(req, res).setError("Tournament does not exist.");
                    }
                    Maps params = Maps.of()
                            .put("title", "Leaderboard")
                            .put("authed", AuthentiationAPI.isAuthenticated(req))
                            .put("tournament", tournament);

                    return APIUtils.render(params.get(), "/templates/leaderboard.vm");
                });

                get("/:tournament/admin", (req, res) -> {
                    Tournament tournament = Tournament.get(MainTest.dbh, req.params("tournament"));

                    if (tournament == null) {
                        return APIResponse.create(req, res).setError("Tournament does not exist.");
                    }

                    Map<Integer, List<Game>> games = tournament.getAllGames().stream()
                            .collect(Collectors.groupingBy(Game::getRound, TreeMap::new, Collectors.toList()));

                    Maps params = Maps.of()
                                          .put("title", "Tournament")
                                          .put("authed", AuthentiationAPI.isAuthenticated(req))
                                          .put("tournament", tournament)
                                          .put("games_live", games);

                    return APIUtils.render(params.get(), "/templates/tournament.vm");
                });
		    });

            get("/players", (req, res) -> APIUtils.render(Maps.of().put("authed", AuthentiationAPI.isAuthenticated(req)).get(), "/templates/players.vm"));


            path("/admin", () -> {
    			get("", (req, res) -> {
    			    Maps params = Maps.of()
                            .put("title", "Admin")
                            .put("authed", AuthentiationAPI.isAuthenticated(req));

    			    return APIUtils.render(params.get(), "/templates/admin.vm");
                });

    			get("/login", (req, res) -> {
                    Maps params = Maps.of()
                            .put("title", "Login")
                            .put("authed", AuthentiationAPI.isAuthenticated(req));

                    return APIUtils.render(params.get(), "/templates/login.vm");
                });
    		});

            path("/api", () -> {

                path("/tournament", () -> {
                    get("/get", TournamentAPI::get);
                    get("/competitors", TournamentAPI::competitors);
                    post("/add", TournamentAPI::add);
                    post("/remove", TournamentAPI::remove);
                    post("/toggle-playing", TournamentAPI::togglePlaying);
                    post("/register", TournamentAPI::register);
                    post("/deregister", TournamentAPI::deregister);
                    post("/create", TournamentAPI::create);
                    post("/recreate", TournamentAPI::recreate);
                    post("/copy-game", TournamentAPI::copyGame);
                    post("/start", TournamentAPI::start);
                    post("/reset", TournamentAPI::reset);
                    post("/lock", TournamentAPI::lock);
                    post("/toggle-signups", TournamentAPI::toggleSignups);
                    post("/report-result", TournamentAPI::reportResult);
                    get("/leaderboard", TournamentAPI::leaderboard);
                });

                post("/authenticate", AuthentiationAPI::authenticate);
                get("/authenticated", AuthentiationAPI::isAuthenticated);
            });

        });

		ApiConfig apiConfig = new ApiConfig().setKey(riotConfig.get("apikey"));
		api = new RiotApi(apiConfig);

//		Summoner summoner = api.getSummonerByName(Platform.EUW, "geomancy");
//		CurrentGameInfo game = api.getActiveGameBySummoner(Platform.EUW, summoner.getId());
//		MatchList list = api.getMatchListByAccountId(Platform.EUW, summoner.getAccountId());
//		list.getMatches().forEach(m -> System.out.println(m.toStringVerbosely(0)));
//		Map<String, LeagueEntry> leagueEntries =
//				api.getLeagueEntriesBySummonerId(Platform.EUW, summoner.getId())
//				.stream()
//				.collect(Collectors.toMap(LeagueEntry::getQueueType, x -> x));
//
//		LeagueEntry soloLeague = leagueEntries.getOrDefault(LeagueQueue.RANKED_SOLO_5x5.toString(), null);
//
//		if (soloLeague != null) {
//			System.out.println(soloLeague.toStringVerbosely(0));
//		}
	}
}
