package com.alexbarter.lol.util;

import com.alexbarter.lol.matchmaking.Game;
import com.alexbarter.lol.matchmaking.Role;

public class ASCIITable {

    public static String create(Game game) {
        StringBuilder builder = new StringBuilder(500);
        String leftAlignFormat = "| %-15s | %-15s |%n";

        builder.append(String.format("+-----------------+-----------------+%n"));
        builder.append(String.format("| Blue Team       | Red Team        |%n"));
        builder.append(String.format("+-----------------+-----------------+%n"));
        for (Role role : Role.values()) {
            builder.append(String.format(leftAlignFormat, game.getBlueTeam().getPlayer(role).getName(), game.getRedTeam().getPlayer(role).getName()));
        }
        builder.append(String.format("+-----------------+-----------------+%n"));

        return builder.toString();
    }
}
