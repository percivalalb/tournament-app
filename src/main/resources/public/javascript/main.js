$(document).ready(function() {
    var tournamentId = $('table#summoners').data('tournament-id')
    var summonersTable = $('table#summoners').DataTable({
        pageLength: 50,
        lengthMenu: [5, 15, 25, 50],
        ajax: {
            url: '/lol/api/tournament/competitors',
            type: 'get',
            dataSrc: 'summoners',
            data: {
                tournamentId: tournamentId,
            }
        },
        columns: [
            { data : "name", render: renderOPGGLink },
            { data : "discord_tag" },
            { data : "elo", render: renderRank },
            { data : "primary_role" },
            { data : "secondary_role" },
            { data : "avoid_role", defaultContent: 'none' },
            { data : "fill", render: renderYesNo },
            { data : "playing", render: renderPlayingToggle },
            { data : null, sortable: false, defaultContent: '<button class="btn btn-sm btn-outline-danger remove">X</button>' },
        ],
    });
    
    var tournamentsTable = $('table#tournaments').DataTable({
        pageLength: 15,
        lengthMenu: [5, 15, 25, 50],
        order: [[ 2, 'desc' ]],
        ajax: {
            url: '/lol/api/tournament/get',
            type: 'get',
            dataSrc: 'tournaments',
        },
        columns: [
            { data : "id", render: renderTournamentLink },
            { data : "created" },
            { data : "type", render: renderTournamentType },
            { data : "participants" },
            { data : null, sortable: false, defaultContent: '<button class="btn btn-sm btn-danger">X</button>' },
        ],
    });
    
    var leaderboardTable = $('table#leaderboard').DataTable({
        pageLength: 10,
        lengthMenu: [5, 10, 15, 25],
        order: [[ 3, 'desc' ], [ 1, 'desc' ], [ 2, 'desc' ]],
        ajax: {
            url: '/lol/api/tournament/leaderboard',
            type: 'get',
            dataSrc: 'leaderboard',
            data: {
                'tournamentId' : $('table#leaderboard').data('tournament-id'),
            },
        },
        columns: [
            { data : "name", render: renderOPGGLink },
            { data : "wins" },
            { data : "mvps" },
            { data : "points" },
        ],
    });
    
    // Reload leaderboard every 15s
    setInterval(function() {
        leaderboardTable.ajax.reload();
    }, 15000);
    
    $('button#randomise-tournament-id').click(function(event) {
        $('input#tournament-id').val(makeTournamentId(7));
    });
    
    $('form#tournament-add').submit(function(event) {
        $.post('/lol/api/tournament/add',
            $(this).serializeArray()
        ).done(function(data) {
            $('input#tournament-id').val('');
            tournamentsTable.ajax.reload();
        }).fail(function(xhr, status, error) {
            alert('Unable to create tournament: ' + xhr.responseJSON.error);
        }).always(function() {
            
        });
        event.preventDefault();
    });
    
    $('form#admin-login').submit(function(event) {
        var redirect = decodeURIComponent(window.location.href.match(/[\?&]redirect=([^&#]*)/)[1]);
        $.post('/lol/api/authenticate',
            $(this).serializeArray()
        ).done(function(data) {
            document.cookie = 'admin=' + data.token + '; expires=Tue, 19 Jan 2038 03:14:07 UTC; path=/';
            window.location.href = redirect;
        }).fail(function(xhr, status, error) {
            alert('Unable to authenticate: ' + xhr.responseJSON.error);
        }).always(function() {
                
        });
        event.preventDefault();
    });
    
    $('form#register-form').submit(function(event) {
        var alertBox = $("#alert-box");
    
        $.post('/lol/api/tournament/register',
            $(this).serializeArray()
        ).done(function(data) {
            window.location.href = data.redirect;
        }).fail(function(xhr, status, error) {
            alertBox.addClass('alert-danger');
            alertBox.removeClass('alert-success');
            alertBox.html(xhr.responseJSON.error);
            //alert('Unable to register: ' + xhr.responseJSON.error);
        }).always(function() {
            alertBox.fadeTo(5000, 500).slideUp(1000, function() {
                alertBox.slideUp(1000);
            });  
        });
        event.preventDefault();
    });
    
    $('form#enter-tournament').submit(function(event) {
        window.location.href = "/lol/tournament/" + $('input#set-tournament-id').val() + '/register';
        event.preventDefault();
    });

    $('table#tournaments tbody').on('click', 'button', function(event) {
        var data = tournamentsTable.row($(this).parents('tr')).data();

        $.post('/lol/api/tournament/remove', {
            'tournamentId': data.id,
        }).done(function(data) {
            $('input#tournament-id').val('');
            tournamentsTable.ajax.reload();
        }).fail(function(xhr, status, error) {
            alert('Unable to remove tournament: ' + xhr.responseJSON.error);
        }).always(function() {
            
        });
        event.preventDefault();
    });
    
    $('table#summoners tbody').on('click', 'button.remove', function(event) {
        var data = summonersTable.row($(this).parents('tr')).data();
        if (!window.confirm("Are you sure you want to remove " +data.name+" from the tournament? This will lose their wins and mvps points!")) { return; }
        $.post('/lol/api/tournament/deregister', {
            'tournamentId': tournamentId,
            'summoner-id': data.summoner_id,
        }).done(function(data) {
            leaderboardTable.ajax.reload();
            summonersTable.ajax.reload();
        }).fail(function(xhr, status, error) {
            alert('Unable to remove player from tournament: ' + xhr.responseJSON.error);
        }).always(function() {
            
        });
        event.preventDefault();
    });
    
    $('table#summoners tbody').on('click', 'button.playing', function(event) {
        var row = summonersTable.row($(this).parents('tr'));
	    var rowData = row.data();
        $.post('/lol/api/tournament/toggle-playing', {
            'tournamentId': tournamentId,
            'summonerId': rowData.summoner_id,
        }).done(function(data) {
             rowData.playing = data.value;
             row.data(rowData).draw();
        }).fail(function(xhr, status, error) {
            alert('Unable to remove tournament: ' + xhr.responseJSON.error);
        }).always(function() {
            
        });
        event.preventDefault();
    });
    
    $('button#start-tournament').click(function(event) {
        $.post('/lol/api/tournament/start', {
            'tournamentId': $(this).data('tournament-id'),
        }).done(function(data) {
            
        }).fail(function(xhr, status, error) {
            alert('Unable to create games: ' + xhr.responseJSON.error);
        });
        event.preventDefault();
    });
    
    $('button#create-games').click(function(event) {
        $.post('/lol/api/tournament/create', {
            'tournamentId': $(this).data('tournament-id'),
        }).done(function(data) {
            var div = $('div#games');
            $('div#colour-key').show();
            
            $('div#games').find('div.game-panel').remove();
            
            for (var gameIndex in data.games) {
                var gameHTML = data.games[gameIndex];
                div.append(gameHTML);
            }
            
            $('button#create-games').html('<i class="fas fa-random"></i> Shuffle Teams');
            $('button#lock-games').prop('disabled', false);
            
//            for (var gameIndex in data.games) {
//                var game = data.games[gameIndex];
//                var str = getMatchHTML(game);
//                div.append(str);
//            }
        }).fail(function(xhr, status, error) {
            alert('Unable to create games: ' + xhr.responseJSON.error);
        }).always(function() {
            
        });
        event.preventDefault();
    });
    
    $('button#lock-games').click(function(event) {
        var gameIds = $('div#games').find('div.game-panel').map(function() {
            return $(this).data('game-id');
        }).get();
        var round = $('button#create-games').data('round');
        
        var panel = $('div#game-accordion');
        var tempPanel = $('div#games');
        $.post('/lol/api/tournament/lock', {
            'tournamentId': $(this).data('tournament-id'),
            'games' : gameIds,
            'round' : round,
        }).done(function(data) {
            $(data.html).insertBefore(panel);
            panel.remove();
            tempPanel.empty();
            $('button#create-games').html('<i class="fas fa-hammer"></i> Create Round ' + (round + 1));
            $('button#create-games').data('round', round + 1);
            $('button#lock-games').prop('disabled', true);
            $('#game-accordion').find('.collapse').not('#game-'+round+'-collapse').collapse('hide');
            $('#game-'+round+'-collapse').collapse('show');
            window.location.href = '#game-'+round+'-heading';
            
        }).fail(function(xhr, status, error) {
            alert('Unable to lock games: ' + xhr.responseJSON.error);
        }).always(function() {
            
        });
        event.preventDefault();
    });
    
    $(document).on('submit', 'form.game-result', function(event) {
        var panel = $('div#game-accordion');
        var tempPanel = $('div#games');
        $.post('/lol/api/tournament/report-result', 
            $(this).serializeArray()
        ).done(function(data) {
            leaderboardTable.ajax.reload();
            $(data.html).insertBefore(panel);
            panel.remove();
            tempPanel.empty();
        }).fail(function(xhr, status, error) {
            alert('Unable to submit game results: ' + xhr.responseJSON.error);
        });
        event.preventDefault();
    });
    
    $('div#games').on('click', 'a.game-refresh', function(event) {
        var panel = $(this).closest('div.game-panel');
        $.post('/lol/api/tournament/recreate', {
            'game-id': panel.data('game-id'),
            'tournament-id': panel.data('tournament-id'),
        }).done(function(data) {
            var div = $('div#games');
            for (var gameIndex in data.games) {
                var gameHTML = data.games[gameIndex];
                $(gameHTML).insertBefore(panel);
                panel.remove();
            }
        }).fail(function(xhr, status, error) {
            alert('Unable to recreate game: ' + xhr.responseJSON.error);
        });
    });
    
    $('div.game-panel').on('click', 'a.game-copy', function(event) {
        var panel = $(this).closest('div.game-panel');
        $.post('/lol/api/tournament/copy-game', {
            'game-id': $(this).closest('div.game-panel').data('game-id'),
        }).done(function(data) {
            var tableData = data.game;
            copyStringToClipboard(tableData);
            console.log(tableData);
        }).fail(function(xhr, status, error) {
            alert('Unable to copy game: ' + xhr.responseJSON.error);
        });
    });
    
    $('select#rank').change(function(event) {
        var tier = $(this).children("option:selected").val();
        if (tier == 'master' || tier == 'grandmaster' || tier == 'challenger') {
            $(this).parent().find('select#division').hide();
            $(this).parent().find('div.lp-input').show();
        } else if (tier == 'unranked') {
            $(this).parent().find('select#division').hide();
            $(this).parent().find('div.lp-input').hide();
        } else {
            $(this).parent().find('select#division').show();
            $(this).parent().find('div.lp-input').hide();
        }
    });
    
    $('div.modal').on('shown.bs.modal', function () {
        $(this).find('.focus-on-modal').first().focus();
    });
    
    $('input#fill').change(function(event) {
        if ($(this).is(':checked')) {
            $('#form-part-avoid').show();
            $('select#avoid-role').prop('disabled', false);
        } else {
            $('#form-part-avoid').hide();
            $('select#avoid-role').prop('disabled', true);
        }
    });
    
    $('input#fill').change(function(event) {
        if ($(this).is(':checked')) {
            $('#form-part-avoid').show();
            $('select#avoid-role').prop('disabled', false);
        } else {
            $('#form-part-avoid').hide();
            $('select#avoid-role').prop('disabled', true);
        }
    });
    
    $('input#rank-manual').change(function(event) {
        if ($(this).is(':checked')) {
            $('#form-rank-part').show();
            $('#form-rank-part').find('input, select').prop('disabled', false);
        } else {
            $('#form-rank-part').hide();
            $('#form-rank-part').find('input, select').prop('disabled', true);
        }
    });
    
    $('input#toggle-signups').change(function(event) {
        var checkbox = $(this);
        $.post('/lol/api/tournament/toggle-signups', {
            'tournamentId': $(this).data('tournament-id'),
        }).done(function(data) {
            checkbox.prop('checked', data.value);
        }).fail(function(xhr, status, error) {
            alert('Unable to toggle signups ' + xhr.responseJSON.error);
        });
        event.preventDefault();
    });
    
    $('div.btn-group').each(function() {
        var group = $(this);
        group.find('input:radio').change(function (event) {
            var checked = $(this).prop('checked');
            
            $(this).closest('label').toggleClass('btn-primary', checked);
            $(this).closest('label').toggleClass('btn-secondary', !checked);
            group.find('input:radio').not($(this)).closest('label').toggleClass('btn-primary', false);
            group.find('input:radio').not($(this)).closest('label').toggleClass('btn-secondary', true);
        });
    });
    $('button#reset-tournament').click(function(event) {
        event.preventDefault();
        if (!window.confirm("Are you sure you want to reset the tournament? This will remove all games and reset players wins and mvps.")) { return; }
    
        $.post('/lol/api/tournament/reset', {
            'tournamentId': $(this).data('tournament-id'),
        }).done(function(data) {
            document.location.reload();
        }).fail(function(xhr, status, error) {
            alert('Unable to reset games: ' + xhr.responseJSON.error);
        });
    });
});

function copyStringToClipboard (str) {
    // Create new element
    var el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
}

function makeTournamentId(length) {
       var result           = '';
       var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
       var charactersLength = characters.length;
       for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }
       return result;
    }

function renderTournamentLink(data, type, row) {
    return type === 'display' ? '<a href="/lol/tournament/'+data+'/admin">'+data+'</a>': data;
}

function renderTournamentType(data, type, row) {
    return type === 'display' ? data == 0 ? '5v5' : (data == 1 ? '1v1' : 'Unknown type') : data;
}

function renderOPGGLink(data, type, row) {
    return type === 'display' ? '<a target="_blank" href="https://euw.op.gg/summoner/userName='+data+'">'+data+'</a>': data;
}

function renderYesNo(data, type, row) {
    return type === 'display' ? data ? 'Yes' : 'No' : data;
}

function renderPlayingToggle(data, type, row) {
    return type === 'display' ? '<button title="Toggle" class="btn btn-sm btn-outline-secondary playing" data-toggle="tooltip" data-placement="right">'+renderYesNo(row.playing, type, row)+'</button>' : data;
}


function renderRank(data, type, row) {
    if (type !== 'display') { return data; }
    switch(row.tier) {
      case 'UNRANKED': return "Unranked";
      case 'IRON':     return "Iron " + renderDivision(row.division);
      case 'BRONZE':   return "Bronze " + renderDivision(row.division);
      case 'SILVER':   return "Silver " + renderDivision(row.division);
      case 'GOLD':     return "Gold " + renderDivision(row.division);
      case 'PLATINUM': return "Platinum " + renderDivision(row.division);
      case 'DIAMOND':  return "Diamond " + renderDivision(row.division);
      case 'MASTER':      return "Master "      + row.lp + "LP";
      case 'GRANDMASTER': return "Grandmaster " + row.lp + "LP";
      case 'CHALLENGER':  return "Challenger "  + row.lp + "LP";
      default: return row.tier;
    }
}
    
function renderDivision(div) {
    return div == 4 ? 'IV' : div == 3 ? 'III' : div == 2 ? 'II' : 'I';
}
