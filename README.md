# Tournament Manager for Warwick Esports

Features
 * Host multiple tournaments at once
 * Simple registration page
   * Fetches player rank via Riot API
   * Primary and secondary role required
   * If fill set can choose a role to avoid
 * 5v5 game matchmaking
   * Tries to put everyone on primary & secondary role
   * Easy to see if games are fair
   * Will create smaller games with remaining players
  

# Live
https://esports.alexbarter.com/lol/tournament/code/leaderboard